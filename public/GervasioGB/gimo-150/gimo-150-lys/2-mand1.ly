\version "2.19.49"

IIMandI =  \relative c'' {
  \clef "treble"
  \tupletSpan 4
  \key g\major
  \time 2/2
%page 11
  \partial 4 d4 |
  d \slashedGrace c8 b16 a b c \tuplet 3/2 { d8[ d d]  d[ d d]} |
  e4 e16 d e fis \tuplet 3/2 { g8[ g g]  g[ fis e]} |
  d4 b16 a b c \tuplet 3/2 { d8[ d d]  d[ c b]} |
  %4
  a4 a16 g a b \tuplet 3/2 { c8[ c c]  c[ b a]} |
  b4 d16 c d e \tuplet 3/2 { g8[ fis e]  g[ fis e]} |
  cis4 e16 d e fis \tuplet 3/2 { e8[ fis g]  g[ fis e]} |
  fis4 \tuplet 3/2 { a8[ g fis]  fis[ e d]  a'[ g fis]} |
  %8
  e4 \tuplet 3/2 { g8[ a b]  a[ g fis]  e[ d cis]} |
  d4 \tuplet 3/2 { a'8[ g fis]  fis[ e d]  a'[ g fis]} |
  e4 \tuplet 3/2 { g8[ a b]  a[ g fis]  e[ d cis]} |
  \tuplet 3/2 { d8[ a' fis]  fis[ d d]  d[ a a]  a[ fis fis]} |
  %12
  fis2 r4 a'4 |
  a4 fis16 e fis g \tuplet 3/2 { a8[ a a]  a[ a a]} |
%page 12
  b4 b,16 a b cis \tuplet 3/2 { d8[ d d]  d[ cis b]} |
  a4 fis'16 e fis g \tuplet 3/2 { a8[ a a]  a[ g fis]} |
  %16
  e4 g16 fis g a \tuplet 3/2 { g8[ g g]  g[ fis e]} |
  d4 fis,16 e fis g \tuplet 3/2 { a8[ a a]  a[ g fis]} |
  e4 e16 d e fis \tuplet 3/2 { g8[ g g]  g[ fis e]} |
  \tuplet 3/2 { d[ fis fis]  fis[ a a]  a[ d d]  d[ f f]} |
  %20
  \tuplet 3/2 { f?[ e d]  f[ e d]  c[ b a]  a[ b c]} |
  \tuplet 3/2 { c[ a a]  a[ c c]  c[ fis fis]  fis[ a a]} |
  \tuplet 3/2 { a[ a, b]  c[ b a]  b[ a b]  d[ c d]} |
  \tuplet 3/2 { g[ d g]  g[ d g]} g4 e |
  %24
  \tuplet 3/2 { d8[ e d]  c[ b a]} g4 \tuplet 3/2 { d'8[ c b]} |
  \tuplet 3/2 { b[ a b]  d[ c b]  b[ c d]  e[ fis g]} |
  \tuplet 3/2 { fis[ e d]  c[ b a]} g2
  \bar "|."
}
