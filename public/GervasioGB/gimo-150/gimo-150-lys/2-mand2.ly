\version "2.19.49"

IIMandII =  \relative c'' {
  \clef "treble"
  \tupletSpan 4
  \key g\major
  \time 2/2
%page 18
  \partial 4 b4 |
  b4 g16 fis g a \tuplet 3/2 { b8[ b b]  b[ b b]} |
  c4 c16 b c d \tuplet 3/2 { e8[ e e]  e[ d c]} |
  b4 g16 fis g a \tuplet 3/2 { b8[ b b]  b[ a g]} |
  %4
  fis4 fis16 e fis g \tuplet 3/2 { a8[ a a]  a[ g fis]} |
  g4 b16 a b c \tuplet 3/2 { b8[ a g]  b[ a g]} |
  e4 cis'16 b cis d \tuplet 3/2 { cis8[ d e]  e[ d cis]} |
  d4 \tuplet 3/2 { fis8[ e d]  a[ g fis]  fis'[ e d]} |
  %8
  b4 \tuplet 3/2 { e8[ fis g]  fis[ e d]  g,[ fis e]} |
  fis4 \tuplet 3/2 { fis'8[ e d]  a[ g fis]  fis'[ e d]} |
  b4 \tuplet 3/2 { e8[ fis g]  fis[ e d]  g,[ fis e]} |
  \tuplet 3/2 { d8[ fis' d]  d[ a a]  a[ fis fis]  fis[ d d]} |
  %12
  d2
%page 19
    r4 fis'4 |
  fis d16 cis d e \tuplet 3/2 { fis8[ fis fis]  fis[ fis fis]} |
  g4 g,16 fis g a \tuplet 3/2 { b8[ b b]  b[ a g]} |
  fis4 d'16 cis d e \tuplet 3/2 { fis8[ fis fis]  fis[ e d]} |
  %16
  cis4 e16 d e fis \tuplet 3/2 { e8[ e e]  e[ d cis!]} |
  d4 d16 cis d e \tuplet 3/2 { fis8[ fis fis]  fis[ e d]} |
  cis4 cis16 b cis d \tuplet 3/2 { e8[ e e]  e[ d cis]} |
  \tuplet 3/2 { d8[ a a]  a[ fis fis]  fis[ a a]  a[ d d]} |
  %20
  \tuplet 3/2 { d[ c b]  d[ c b]  a[ gis a]} a4 |
  \tuplet 3/2 { a8[ fis fis]  fis[ a a]  a[ c c]  c[ fis fis]} |
  \tuplet 3/2 { fis[ fis, g]  a[ g fis]  g[ fis g]  b[ a b]} |
  \tuplet 3/2 { d[ b d]  d[ b d]} d4 c |
  %24
  \tuplet 3/2 { b8[ c b]  a[ g fis]} g4 \tuplet 3/2 { b8[ a g]} |
  \tuplet 3/2 { g[ fis g]  b[ a g]  g[ a b]  c[ d e]} |
  \tuplet 3/2 { d[ c b]  a[ g fis]} g2 % the g2 was e2 in the manuscript
  \bar "|."
}
