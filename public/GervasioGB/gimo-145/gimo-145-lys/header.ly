\version "2.19.49"

\header {
  title = "Sonata per Mandolino e Basso"
  mutopiatitle = "Sonata per Mandolino e Basso (Gimo 145)"
  composer = "Giovanni Battista Gervasio (c.1725-c.1785)"
  mutopiacomposer = "GervasioGB"
  mutopiainstrument = "Mandolin and Bass"
  date = "c.1760"
  mutopiadate = "c. 1760"
  source = "Gimo Music Collection"
  style = "Baroque"
  license = "Public Domain"
  maintainer = "Erik Sandberg"
  maintainerEmail = "ersa9195@student.uu.se"
  instrument = \Instrument

 footer = "Mutopia-2013/04/10-329"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}
