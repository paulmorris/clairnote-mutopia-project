\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\header {
  enteredby = 	"Allen Garvin"
  maintainer = 	"Allen Garvin"
  license = "Public Domain"
  filename = 	"anna-magdalena-20a.ly"
  title = 	"Aria"
  opus = 	"BWV 515"
  composer =	"Johann Sebastian Bach (1685-1750)"
  style =	"Baroque"
  source =	"Bach-Gesellschaft"

  mutopiainstrument = "Harpsichord, Piano, Clavichord"
  mutopiatitle =      "Aria"
  mutopiacomposer =   "BachJS"
  mutopiaopus =       "BWV 515"
  maintainerEmail =   "AGarvin@tribalddb.com"

 footer = "Mutopia-2013/01/22-78"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

\include "english.ly"

voiceone =  \relative c' {
  \key d \minor
  \time 3/4
  \clef "treble"

  \repeat "volta" 2 {
    d4 d d |
    g4.(  a16[  bf)] a4 |
    g  f8[ e] a4 |
    f  e8[ f] d4 |
    f f f |
    a4.(  bf16[  c)] f,4 |
    g e2 |
    f2. |
  }
  \repeat "volta" 2 {
    g4 g g |
    g2 e4 |
    a  g8[ f e d] |
    cs2. |
    a4 cs e |
    g4.  a16[ bf] a4 |
    f(  d) cs |
    d2. |
  }
}

voicetwo =  \relative c {
  \clef "bass"
  \key d \minor
  \time 3/4

  \repeat "volta" 2 {
    d4 d' c |
    bf e, f |
    g a a, |
    d a f |
    d d' bf |
    f' c d |
    bf c c, |
    f2. |
  }
  \repeat "volta" 2 {
    c''4 e, g |
    b g c |
    f,2 g4 |
    a  g8[ f e d] |
     cs[ b a b cs d] |
    e4 d cs |
    d f a |
    d, a d, |
  }
}
   
\score {
   \context GrandStaff << 
    \context Staff = "one" <<
      \voiceone
    >>
    \context Staff = "two" <<
      \voicetwo
    >>
  >>

  \layout{}
  
  \midi {
    \tempo 4 = 105
    }
}

