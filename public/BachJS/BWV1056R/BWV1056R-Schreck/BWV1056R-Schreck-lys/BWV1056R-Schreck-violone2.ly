\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "violine2.ily"

instrument = "Violine II"
\include "header.ily"
\header {piece = "Largo"}
\score {
  \new Staff {\largoViolineII}
  \layout { }
  \midi {\tempo 4 = 30}
}
