\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "viola.ily"

instrument = "Viola"
\include "header.ily"
\header {piece = "Largo"}
\score {
  \new Staff {\largoViola}
  \layout { }
  \midi {\tempo 4 = 30}
}
