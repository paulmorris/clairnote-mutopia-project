\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "violine1.ily"

instrument = "Violine I"
\include "header.ily"
\header {piece = "Largo"}
\score {
  \new Staff {\largoViolineI}
  \layout { }
  \midi {\tempo 4 = 30}
}
