\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "violoncello.ily"

instrument = "Violoncello"
\include "header.ily"
\header {piece = "Largo"}
\score {
  \new Staff {\largoVioloncello}
  \layout { }
  \midi {\tempo 4 = 30}
}
