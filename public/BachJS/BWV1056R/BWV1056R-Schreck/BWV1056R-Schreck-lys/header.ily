\version "2.19.49"

\header {
  title = "Concerto in G minor"
  composer = "Johann Sebastian Bach"
  opus = "BWV 1056R"
  date = "1730–1738?"
  source = "Schreck G., ca. 1904 (reconstruction); Leipzig: C.F. Peters, n.d. (ca. 1904), plate 8982; IMSLP #363999"
  style = "Baroque"

  instrument = \instrument

  mutopiacomposer = "BachJS"
  mutopiainstrument = "Violin, Viola, Basso Continuo, String Ensemble"

  maintainer = "Júda Ronén"
  maintainerWeb = "http://me.digitalwords.net/"
  license = "Public Domain"
  lastupdated = "2016-05-08"

  moreInfo = "Only the the second movement has been typeset by now. Feel free to complete the other movements, but please <a href=\"http://me.digitalwords.net/\">contact</a> me before for coordination."

 footer = "Mutopia-2016/05/11-2116"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}
