\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "violino.ily"

instrument = "Violino"
\include "header.ily"
\header {piece = "Largo"}
\score {
  \new Staff {\largoViolino}
  \layout { }
  \midi {\tempo 4 = 30}
}
