\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\header {
  enteredby = 	"Allen Garvin"
  maintainer = 	"Allen Garvin"
  maintainerEmail = "AGarvin@tribalddb.com"
  license = "Public Domain"
  filename = 	"anna-magdalena-03.ly"
  title = 	"Menuet"
  opus = 	"BWV Anh. 113"
  composer =   "J. S. Bach (1685-1750)"
  style =	"Baroque"
  source =	"Bach-Gesellschaft"

  mutopiainstrument = "Harpsichord, Piano, Clavichord"
  mutopiacomposer =   "BachJS"
  mutopiatitle =      "Menuet"
  mutopiaopus =       "BWV Anh. 113"

 footer = "Mutopia-2013/01/06-74"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

voiceone =  \relative c'' {
  \clef "treble"
  \time 3/4
  \key f \major

  \repeat "volta" 2 {
    c4  d16 e f8 e4 |
     \tuplet 3/2 {f8( e  d)} c2 | %A grace might be missing here
     \tuplet 3/2 { d8 ees d }  \tuplet 3/2 {c d c }  \tuplet 3/2 {bes c bes} |
    \grace bes8 a2 g4 |
     a8 c f, c' g c |
     a c bes c g c |
     a c f, c' g c |
     a c bes c g c |
     a c d e f d |
     c b a g  c16( d c  b) |
     c8 f e4  d16\trill c d8 |
    c2. |
  }
  \repeat "volta" 2 {
    g'4  f8 e f d |
     e g bes4 r |
    a  g8 f e d |
    \grace d8 cis2. |
     cis8 d e cis d e |
    a,4 a a |
     cis8 d e cis d e |
    g,4 g g |
     cis8 d e cis d e |
     a, d c! bes a g |
    f4 g e |
    d2. |
     c'8 f e f ees f |
     a f d f c f |
     d g fis g f g |
     bes g e g c, bes |
     a f' g a g f |
     bes, g' a bes a g |
     c, c' bes a g a |
    \grace g8 f2. |
  }
}

voicetwo =  \relative c {
  \time 3/4
  \key f \major
  \clef "bass" 

  \repeat "volta" 2 {
    f4 a g |
    a  f8 g a f |
    bes4 c c, |
    f  c8 d e c |
    f4 a, g |
    f g e |
    f a g |
    f g e |
    f f' d |
    g e a |
    f g g, |
    c2. |
  }
  \repeat "volta" 2 {
    c4 g' g, |
    c  e8 d e c |
    f4 bes g |
    a e cis |
    a a' e |
     cis8 d e cis d e |
    a,4 a a |
     cis8 d e cis d e |
    g,4 g g |
    f f' g |
    a2 a,4 |
     d8 d' c bes a g |
    a2 g4 |
    f bes a |
    bes2 a4 |
    g c e, |
    f r r |
    g r r |
    a bes c |
    f,2. |
  }
}
  
    
\score {
   \context GrandStaff << 
    \context Staff = "one" <<
      \voiceone
    >>
    \context Staff = "two" <<
      \voicetwo
    >>
  >>

  \layout{}
  
  \midi {
    \tempo 4 = 120
    }


}

