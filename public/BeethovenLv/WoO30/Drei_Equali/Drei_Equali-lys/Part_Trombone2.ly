\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "Definitions.lyi" 

instrument = "Trombone 2"
staffInstrument = "Trom 2"
useClef = {\clef tenor}

\include "Notes_Trombone2_Movement1.lyi"
notesMvtI = \TromTwoMvtOne

\include "Notes_Trombone2_Movement2.lyi"
notesMvtII = \TromTwoMvtTwo

\include "Notes_Trombone2_Movement3.lyi"
notesMvtIII = \TromTwoMvtThree

\include "MakePart.lyi"
