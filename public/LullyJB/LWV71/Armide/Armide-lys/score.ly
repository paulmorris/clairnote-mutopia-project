\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\header {
 mutopiatitle = "Acte II, Scene III from Armide"
 mutopiacomposer = "LullyJB"
 mutopiapoet = "P. Quinault (1635-1688)"
 mutopiaopus = "LWV 71"
 mutopiainstrument = "Voice (Tenor), String Ensemble, Basso Continuo"
 source = "Ballard, 1686"
 style = "Baroque"
 license = "Public Domain"
 maintainer = "Björn Sothmann"
 maintainerEmail = "bjoern.sothmann@rub.de"
 maintainerWeb = "http://www.thp.uni-due.de/~bjoerns/"
 
 title="ACTE II SCENE III."
 subsubtitle="Renaud seul."
 %footer="Created 9.2.05 by Björn Sothmann Bjoernsothmann@aol.com"
 
 footer = "Mutopia-2013/02/21-532"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

\include "Dessus.ly"
\include "Haute.ly"
\include "Taille.ly"
\include "Quinte.ly"
\include "Renaud.ly"
\include "Basse.ly"

#(set-global-staff-size 16)

\score{
{
	<<
	\new Staff = Dessus \dessus
	\new Staff = Haute \haute
	\new Staff = Taille \taille
	\new Staff = Quinte \quinte
	
	\new Staff = Renaud \Renaud
	\addlyrics \RenaudL
	
	\new Staff = Basse \basse
	>>	
}
	\layout{}

}

\score{
{
	\applyMusic #unfold-repeats
	<<
	\new Staff = Dessus 
	<<\dessus
	\set Staff.midiInstrument = #"violin">>
	\new Staff = Haute
	<<\haute
	\set Staff.midiInstrument = #"violin">>
	\new Staff = Taille
	<<\taille
	\set Staff.midiInstrument = #"violin">>
	\new Staff = Quinte
	<<\quinte
	\set Staff.midiInstrument = #"violin">>
	\new Staff = Renaud 
	<<\Renaud
	\set Staff.midiInstrument = #"oboe">>
	\new Staff = Basse
	<<\basse
	\set Staff.midiInstrument = #"harpsichord">>
	>>	
}
	
  \midi {
    \tempo 4 = 80
    }
}
