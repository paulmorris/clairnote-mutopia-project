\header{
filename =	 "corni-part.ly"
enteredby =	"Felix Braun"
license = "Public Domain"
}

\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "sntglobal.ly"
\include "corni.ly"
\include "sntheader.ly"

\header {
	instrument = "Corni"
}

\score {
	\corniStaff
	\include "sntpart-paper.ly"
	\include "sntmidi.ly"
}
