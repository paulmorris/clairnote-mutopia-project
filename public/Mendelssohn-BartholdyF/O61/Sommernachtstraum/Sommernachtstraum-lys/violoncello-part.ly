\header{
filename =	 "violoncello-part.ly"
enteredby =	"Felix Braun"
license = "Public Domain"
}

\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "sntglobal.ly"
\include "violoncello.ly"
\include "sntheader.ly"

\header {
	instrument = "Violoncello"
}

\score {
	\violoncelloStaff
	\include "sntpart-paper.ly"
	\include "sntmidi.ly"
}
