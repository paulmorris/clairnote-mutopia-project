\header{
filename =	 "oboi-part.ly"
enteredby =	"Felix Braun"
license = "Public Domain"
}

\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "sntglobal.ly"
\include "oboi.ly"
\include "sntheader.ly"

\header {
	instrument = "Oboi"
}

\score {
	\oboiStaff
	\include "sntpart-paper.ly"
	\include "sntmidi.ly"
}
