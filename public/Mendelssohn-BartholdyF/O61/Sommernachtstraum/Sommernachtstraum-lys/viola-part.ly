\header{
filename =	 "viola-part.ly"
enteredby =	"Felix Braun"
license = "Public Domain"
}

\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "sntglobal.ly"
\include "viola.ly"
\include "sntheader.ly"

\header {
	instrument = "Viola"
}

\score {
	\violaStaff
	\include "sntpart-paper.ly"
	\include "sntmidi.ly"
}
