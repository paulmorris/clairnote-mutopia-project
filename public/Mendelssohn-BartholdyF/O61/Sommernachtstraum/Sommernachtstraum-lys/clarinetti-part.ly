\header{
filename =	 "clarinetti-part.ly"
enteredby =	"Felix Braun"
license = "Public Domain"
}

\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "sntglobal.ly"
\include "flaucla.ly"
\include "sntheader.ly"

\header {
	instrument = "Clarinetti in A"
}

\score {
	\clarinettiStaff
	\include "sntpart-paper.ly"
	\include "sntmidi.ly"
}
