\header{
filename =	 "violino-1-part.ly"
enteredby =	"Felix Braun"
license = "Public Domain"
}

\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "sntglobal.ly"
\include "violino-1.ly"
\include "sntheader.ly"

\header {
	instrument = "Violino I"
}

\score {
	\violinoBStaff
	\include "sntpart-paper.ly"
	\include "sntmidi.ly"
}
