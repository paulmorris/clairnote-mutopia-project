\header{
filename =	 "contrabasso-part.ly"
enteredby =	"Felix Braun"
license = "Public Domain"
}

\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "sntglobal.ly"
\include "contrabasso.ly"
\include "sntheader.ly"

\header {
	instrument = "Contrabasso"
}

\score {
	\contrabassoStaff
	\include "sntpart-paper.ly"
	\include "sntmidi.ly"
}
