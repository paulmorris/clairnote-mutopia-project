\header{
filename =	 "sntglobal.ly"
maintainer =	 "Felix Braun"
license = "Public Domain"
}

\version "2.19.49"

global =  {
	\time 6/8
	\key a\minor
}
