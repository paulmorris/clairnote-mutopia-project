\header{
filename =	"fagotti-part.ly"
enteredby =	"Felix Braun"
license = "Public Domain"
}

\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "sntglobal.ly"
\include "fagotti.ly"
\include "sntheader.ly"

\header {
	instrument = "Fagotti"
}

\score {
	\fagottiStaff
	\include "sntpart-paper.ly"
	\include "sntmidi.ly"
}
