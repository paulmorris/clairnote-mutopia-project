\header{
filename =	"violino-2-part.ly"
enteredby =	"Felix Braun"
license = "Public Domain"
}

\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "sntglobal.ly"
\include "violino-2.ly"
\include "sntheader.ly"

\header {
	instrument = "Violino II"
}

\score {
	\violinoCStaff
	\include "sntpart-paper.ly"
	\include "sntmidi.ly"
}
