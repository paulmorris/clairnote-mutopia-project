\version "2.19.49"

\header{
filename =	 "sntheader.ly"
composer =	 "Felix Mendelssohn-Bartholdy (1809-1847)"
mutopiacomposer= "Mendelssohn-BartholdyF"
title =	 	 "Ein Sommernachtstraum"
subtitle =	 "No 5 - Allegro appassionato"
mutopiatitle =	 "Ein Sommernachtstraum - No.5"
opus =	 	 "Op. 61"
mutopiaopus =	 "O 61"
maintainer =	 "Felix Braun"
license = "Public Domain"
maintainerEmail = "fbraun@atdot.org"
mutopiainstrument = "Orchestra: Flute, Oboe, Clarinet, Bassoon, French Horn, Violins, Viola, 'Cello, Double Bass"
style		  = "Romantic"
source =	 "Unknown"

 footer = "Mutopia-2013/02/17-80"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}
