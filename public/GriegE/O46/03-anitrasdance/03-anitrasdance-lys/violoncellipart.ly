\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\include "header.ly"
\include "violoncelli.ly"

\header { instrument = "Violoncelli I, II" }

\score {
  <<
    \new Staff \with \violoncelliOneStaffSettings
    { \violoncelliI \bar "|." }
    \new Staff \with \violoncelliTwoStaffSettings
    { \violoncelliII }
  >>
  \layout {
    indent = 2.5 \cm
    short-indent = 1 \cm
  }

  \midi {
    \tempo 4 = 160
  }
}
