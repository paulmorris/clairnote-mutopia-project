\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\include "header.ly"
\include "violiniI.ly"
\header { instrument="Violini I" }

\score {
  \new Staff \with \violiniOneStaffSettings
  {
    \partcombine \violinioneI \violinioneII
    \bar "|."
  }
  \layout {
    indent = 3 \cm
    short-indent = 1.5 \cm
  }

  \midi {
    \tempo 4 = 160
  }
}
