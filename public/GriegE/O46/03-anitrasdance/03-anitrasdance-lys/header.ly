\version "2.19.49"

\header {
 mutopiatitle = "Anitra's Dance (Tanz) from Peer Gynt Suite I"
 mutopiacomposer = "GriegE"
 mutopiapoet = "Henrik Ibsen"
 mutopiaopus = "O 46, No. 3"
 mutopiainstrument = "String Ensemble: Violins, Violas, Cello, Double Bass, Triangle"
 date = "1874-76 - rescored by Grieg 1886"

title = "Anitra's Dance (Tanz)"
subtitle = "La Danse D'Anitra"
composer = "Edvard Grieg (1843 - 1907)"
meter = "Tempo di Mazurka"
opus = "Op. 46 No. 3 - from Peer Gynt Suite I"
license = "Public Domain"
source = "Kunkel Brothers 1891, 1893, Best National Music Co. 188?"
style = "Romantic"
filename = "anitrasdance.ly"
maintainer = "Deborah Lowrey"
maintainerEmail = "drlowrey@karmaresources.com"
maintainerWeb = "http://www.karmaresources.com/"
lastupdated = "2014/01/15" %By Francisco Vila, paconet.org@gmail.com

 footer = "Mutopia-2014/02/25-281"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}


crM = _\markup{\italic "cresc."}
wcrM = _\markup{\whiteout\italic "cresc."}
piucM = _\markup{\italic "più cresc."}
diM = _\markup{\italic "dim."}
wdiM = _\markup{\whiteout\italic "dim."}
