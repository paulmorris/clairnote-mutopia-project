\version "2.19.49"

% Note: This movement has identical parts for mandolin 1 & 2.

IIMand =  \relative c''' {
  \clef "treble"
  \key g\minor
  \time 2/4
  \tupletSpan 8
  % end beams on eighths by default
  \set Timing.baseMoment = #(ly:make-moment 1/8)
%  \set Timing.beamExceptions = #'((end . (
%    ((1 . 32) . (2 2 2 2 2 2 2 2))
%  )))

  %1 page 9
  g8 <<g4 \\ bes,>> \tuplet 3/2 {bes'16 a g} |
  d8 <d fis,>4 \tuplet 3/2 {es16( d  c)} |
  bes8 <<bes4 \\ g>> \tuplet 3/2 {c16 bes a} |  g8[ d] g,4 |
  g''8 <<g4 \\ bes,>> \tuplet 3/2 {bes'16 a g} |
  d8 <<d4 \\ fis,>> \tuplet 3/2 {es'16 d c} |
  bes8 <<bes4 \\ g>> \tuplet 3/2 {c16 bes a} |
   g8[ d]  g,[ \tuplet 3/2 {bes''16 a g]} |
  %9
  f8 <<f4 \\ a,>> \tuplet 3/2 {g'16 f es} |
  d8 <<d4 \\ f,>> \tuplet 3/2 {f'16 es d} |
  c8 \tuplet 3/2 {es16[ d c] g'[ f es] d[ c bes] |
  a g f} f4 \tuplet 3/2 {a'16 g f |
  bes a g} \grace f8 es4 \tuplet 3/2 {g16 f es |
  %14 page 10
  a g f} d4 \tuplet 3/2 {f16 es d | g f es d es c}  bes8[ a] |
  bes4. \tuplet 3/2 {d16 c bes} |
  f'8 <<f4 \\ b,>> \tuplet 3/2 {as'16 g f |
  es d c} c4 \tuplet 3/2 {es16 d c} |
  g'8 <g cis,>4 \tuplet 3/2 {bes16 a g |
  fis e d} d4 \tuplet 3/2 {es16 d c} |
  %21
  <bes d,>8 \tuplet 3/2 {d16 c bes} <a d,>8 \tuplet 3/2 {es'16 d c} |
  \grace c8 bes8. a32 bes  a8[ \tuplet 3/2 {es'16 d c]} |
  <bes d,>8 \tuplet 3/2 {d16 c bes} <a d,>8 \tuplet 3/2 {es'16 d c} |
  <g' bes, d,>8. fis32(  g) \tuplet 3/2 {a16[ g fis] es[ d c]} |
  <bes d, g,>8. c32(  d) \tuplet 3/2 {es16[ d c] bes[ a g]} |
  d8. g32(  a) a4 |
  %27 page 11
  <g' bes, d,>8. fis32 g \tuplet 3/2 {a16[ g fis] es[ d c]} |
  %28 Note: Dotted eighths in 28,29 are not dotted in the manuscript
  <bes d, g,>8. c32 d \tuplet 3/2 {es16[ d c] bes[ a g]} |
  d8. g32 a a4 |  g8[ d] g,4 \bar "|."
}

