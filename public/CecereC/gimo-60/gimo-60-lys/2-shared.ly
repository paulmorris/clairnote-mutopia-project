\version "2.19.49"

% Note: This file contains notes shared between parts. Almost all are marked with a // in each bar

%m+1+2
IISharedA =  \relative c'' {
  \partial 8 b8 |
  %1 page 14
  e e ~ \tuplet 6/4 {e16 b' a gis fis eis} \tuplet 3/2 {fis eis fis} fis8 ~
    \tuplet 6/4 {fis16 a gis fis e dis} |
  \tuplet 3/2 {e dis e} e8 ~ \tuplet 6/4 {e16 e fis gis fis e}
    \tuplet 3/2 {fis dis b} b8 ~ \tuplet 6/4 {b16 fis' gis a gis fis} |
  \tuplet 3/2 {gis16 fis e} e8 r b'16. b32
    \grace {a16[ b]} cis16. b32 a16. gis32 fis8 e |
}
%m+1
IISharedB =  \relative c'' {
  %4
  e dis ~ \tuplet 6/4 {dis16 fis gis a gis fis
    gis b b e, gis gis fis a a dis, fis fis} |
}
%m+1+2
IISharedC =  \relative c'' {
  %5 page 15
  e8. dis32 cis b8 ais b16. cis32 dis16. e32 fis16. gis32 a16. fis32 |
  \tuplet 6/4 {gis16 b a gis fis e fis a gis fis e dis}
}
%1+2
IISharedD =  \relative c'' {
   b8[ e, fis fis] |
  %12 page 16 Note: "b b~" was "b b" in manuscript
  b b~ \tuplet 6/4 {b16 fis' e dis cis b} \tuplet 3/2 {cis e dis} cis8 ~
}
%m+1+2
IISharedE =  \relative c'' {
  \tuplet 6/4 {cis16 e dis cis b ais} |
  %13 page 16
  \tuplet 3/2 {b16 ais b} b8 ~ \tuplet 6/4 {b16 b cis dis cis b}
    \tuplet 3/2 {cis ais fis} fis8 ~ \tuplet 6/4 {fis16 cis' dis e dis cis} |
  \tuplet 3/2 {dis16 cis b} b8 r fis'16. fis32 \grace {e16[ fis]} gis16. fis32 e16. dis32 cis8 b |
}
%m+1
IISharedF =  \relative c'' {
  %15 page 16
  b8 ais \tuplet 6/4 {r16 cis dis e dis cis dis fis b b, gis' e} dis8 cis |
  %16 page 17
  \tuplet 6/4 {b16 fis e dis! e fis}
}
%1+2
IISharedG =  \relative c'' {
  %17
  ais8 ais ais ais b b dis, dis |
  e e e e dis! dis b' b |
  cis cis ais ais b b dis, dis |
  e gis a b
}
%m+1+2
IISharedH =  \relative c'' {
  %23 page 18
  \tuplet 6/4 {e16 b' a gis fis eis} \tuplet 3/2 {fis eis fis} fis8 ~
    \tuplet 6/4 {fis16 a gis fis e dis} |
  \tuplet 3/2 {e16 dis e} e8 ~ \tuplet 6/4 {e16 e fis gis fis e}
    \tuplet 3/2 {dis fis b} b,8 ~
}
%m+1
IISharedI =  \relative c'' {
  \tuplet 6/4 {b16 fis' gis a gis fis |
  %24
  gis b b e, gis gis fis a a dis, fis fis}
}
%m+1+2
IISharedJ =  \relative c'' {
  e8. dis32 cis b8 a |
}
%m+1
IISharedK =  \relative c'' {
  %25 page 19
  \tuplet 6/4 {gis16 fis e cis' b a} gis8 fis
}
