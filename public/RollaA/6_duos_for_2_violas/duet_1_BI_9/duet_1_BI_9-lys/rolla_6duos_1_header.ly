\version "2.19.49"
\header {
    title = "6 Duetti a Due Viole"
    opus = "Duetto No.1. (BI 9)"
    composer = "Allessandro Rolla (1757 - 1841)"
    mutopiatitle = "6 Duetti a Due Viole: No. 1"
    mutopiacomposer = "RollaA"
    mutopiainstrument = "Viola"
    source = "Manuscripts downloaded from http://imslp.org/"
    style = "Baroque"
    license = "Public Domain"
    maintainer = "Huw Richards"
    maintainerEmail = "huw.richards@eng.cam.ac.uk"

 footer = "Mutopia-2013/12/01-1880"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

\paper {
  % add space between composer/opus markup and first staff
  markup-system-spacing.padding = #3
  % add a little space between composer and opus
  markup-markup-spacing.padding = #1.2
  top-margin = #8
  bottom-margin = #10
}
