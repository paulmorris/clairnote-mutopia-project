\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

%#(set-default-paper-size "a4")
%#(set-default-paper-size "letter")
	
\paper{
	indent = 10. \mm
	line-width = 189. \mm
	top-margin = 20\mm
	bottom-margin = 13\mm
	
	system-system-spacing.basic-distance = #15
	markup-system-spacing.basic-distance = #6
	top-system-spacing.basic-distance = #12
	
	evenHeaderMarkup = \oddHeaderMarkup
}

\header{
  title =	 "Konzert Nr. 3 Es dur"
  subtitle =     \markup{ \center-column { "für Horn und Orchester" " " \normal-text "Horn in F" } }
  composer =	 \markup{ \override #'( baseline-skip . 2.5 ) \right-column { \caps "Wolfgang Amadeus Mozart" "(1756-1792)" }}
  opus =         "KV 447"

  enteredby =	 "HWN"
  %updated from v1.5.57 to v2.16.1 by Javier Ruiz-Alma (2014/01/18)
  license = "Public Domain"
  editor = "Henri Kling"
  mutopiatitle = "Horn Concerto No. 3"
  mutopiacomposer = "MozartWA"
  mutopiaopus = "KV 447"
  mutopiainstrument = "Horn in F"
  style = "Classical"
  maintainer = "hanwen@cs.uu.nl"
  maintainerEmail = "hanwen@cs.uu.nl"
  maintainerWeb = "http://www.cs.uu.nl/~hanwen/"
  lastupdated = "2014/01/18"
  source = "Edition Breitkopf 2563 (Ed. Henri Kling)"

 footer = "Mutopia-2014/03/28-25"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

%{

This is the Mozart 3 for horn.  It's from an Edition Breitkopf EB
2563, edited by Henri Kling. Henri Kling (1842-1918) was a horn
virtuoso that taught in Geneva. 

%}

\include "mozart-hrn3-allegro.ly"
\include "mozart-hrn3-romanze.ly"
\include "mozart-hrn3-rondo.ly"



