\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
#(set-global-staff-size 13)

\include "corni_di_bassetto.ily"
\include "fagotti.ily"
\include "trombe.ily"
\include "timpani.ily"
\include "violino-1.ily"
\include "violino-2.ily"
\include "viola.ily"
\include "choir.ily"
\include "organo.ily"

\header {
  title = "2. Dies Irae"
  opus = "K 626"
  composer = "W. A. Mozart (1756-1791)"

  mutopiatitle = "Dies Irae"
  mutopiacomposer = "MozartWA"
  mutopiaopus = "KV 626"
  mutopiainstrument = "Voice (SATB), Orchestra: Basset Horns in F, Bassoon, Trumpet in D, Timpani, Violins, Viola, Cello, Organ/Bass"
  date = "1791"
  source = "Breitkopf & Haertel, 1877"
  style = "Classical"
  license = "Public Domain"
  maintainer = "Martin Norbäck"
  % updated by Francisco Vila <paconet.org@gmail.com>, 2014
  maintainerEmail = "d95mback@dtek.chalmers.se"
  maintainerWeb = "http://norpan.org/cvs/K626/"
  lastupdated = "2014/Feb/22"

  moreInfo = "<p>The Breitkopf &amp; Haertel edition used as the source was published in 1877, edited by Johannes Brahms (1833-1897).</p><p>It is also called the &quot;Süssmayr&quot; version, because Franz Xaver Süssmayr (1766-1803) took credit for completing the Requiem after Mozart's death. There is much rumor as to who else was involved.</p>"

 footer = "Mutopia-2014/03/24-364"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

\score {
  <<
    \context StaffGroup = "all" <<
      \corniDiBassettoStaff
      \fagottiStaff
      \trombeStaff
      \timpaniStaff
      \violinoAStaff
      \violinoBStaff
      \violaStaff
    >>
    \choirStaff
    \organoStaff
  >>

  \midi {
    \tempo 2 = 90
  }


  \layout {  }
}

\paper{
  indent = 2\cm
  short-indent = 1\cm

}
