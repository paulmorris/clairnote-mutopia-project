\version "2.19.49"

\header {
    title = "String Quartet KV. 465 (nr. 19) ``Dissonances''"
    subtitle = "for 2 violins, viola and cello"
    composer = "W. A. Mozart (1756-1791)"
        
    mutopiatitle = "String Quartet KV. 465 (nr. 19) \"Dissonances\""
    mutopiacomposer = "MozartWA"
    mutopiaopus = "KV 465"
    mutopiainstrument = "String Quartet: Two Violins, Viola, 'Cello"
    date = "14th January 1785 (Wien)"
    source = "Breitkopf und Härtel (1882)"
    style = "Classical"
    license = "Public Domain"
    maintainer = "Maurizio Tomasi"
    maintainerEmail = "zio_tom78@hotmail.com"

 footer = "Mutopia-2013/08/11-279"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

% Some useful macros

cresc = \markup { \italic cresc.}
decresc = \markup { \italic decresc.}

tupletNum = \revert TupletNumber.stencil

noTupletNum = \override TupletNumber.stencil = ##f

noTupletBracket = \override TupletBracket.bracket-visibility = ##f 

parentP = \markup {\center-align \concat { \bold { \italic ( }
                           \dynamic p \bold { \italic ) } } }

% General markings and annotations for each movement

markingsI =  {
    \tempo "Adagio."
    s2.*22
    \tempo "Allegro."
}


markingsII =  {
    \tempo "Andante cantabile."
}


markingsIII =  {
    \tempo "Minuetto."
    s4
    s2.*62

    s2
    \tempo "Trio."
}

markingsIIIbis =  {
    s4
    s2.*102
    s4
    s4_ \markup{"M.D.C."}
    \bar "|."
}


markingsIV =  {
    \tempo "Allegro."
}
