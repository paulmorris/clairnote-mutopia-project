\version "2.19.49"


\header {
    title = "String Quartet KV. 387 (nr. 14)"
    subtitle = "for 2 violins, viola and cello"
    composer = "W. A. Mozart (1756-1791)"
    opus = "KV. 387"
        
    mutopiatitle = "String Quartet KV. 387 (nr. 14)"
    mutopiacomposer = "MozartWA"
    mutopiaopus = "KV 387"
    mutopiainstrument = "String Quartet: Two Violins, Viola, 'Cello"
    date = "December 1782"
    source = "Breitkopf und Härtel (1882)"
    style = "Classical"
    license = "Public Domain"
    maintainer = "Maurizio Tomasi"
    maintainerEmail = "zio_tom78@hotmail.com"

 footer = "Mutopia-2013/07/07-245"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

% General markings and annotations for each movement

globalI = {
    \time 4/4
    \partial 8
}

KeyI =  \key g \major


globalII = {
    \time 3/4
}

KeyII =  \key g \major


globalIII = {
    \partial 4
    \time 3/4
}

KeyIII =  \key c \major


globalIV = {
    \time 4/4
}

KeyIV =  \key g \major



markingsI =  {
    \tempo "Allegro vivace assai."
}

markingsII =  {
    s4^ \markup{"MINUETTO. Allegro."} s2
    s2.*92
    \mark "TRIO." s2.
    s2.*51
    s4^ \markup{\center-column {{\italic "M.D.C."} {\italic "senza replica"}}}
}

markingsIII =  {
    \tempo "Andante cantabile."
    s4
    s2.*106
    \bar "|."
}

markingsIV =  {
    \tempo "Molto Allegro."
    s1*298
    \bar "|."
}
