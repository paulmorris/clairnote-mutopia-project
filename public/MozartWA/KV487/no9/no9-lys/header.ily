\version "2.19.49"

\header {
    title = "12 duets"
    composer = "W. A. Mozart (1756-1791)"
    opus = "K.V. 487"

    mutopiatitle = "12 Duets (No. 9: Menuetto)"
    mutopiacomposer = "MozartWA"
    mutopiaopus = "KV 487"
    mutopiainstrument = "Horn Duet"
    source = "See the collection's information page"
    style = "Classical"
    license = "Public Domain"
    maintainer = "Han-Wen Nienhuys"
    maintainerEmail = "hanwen@xs4all.nl"

 footer = "Mutopia-2014/09/07-398"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

\paper { linewidth = 18.0\cm }
