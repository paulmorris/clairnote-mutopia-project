% Don Giovanni part.
\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

Instrument = "Don Giovanni"
\include "header.ly"
#(set-global-staff-size 20)
\include "notes.ly"

\score {
    \context Staff = DonGiovanni \DonGiovanniStaff
  \addlyrics \DonGiovanniLyrics

  \layout {}
  % no midi here, use score.ly for midi output.
}
