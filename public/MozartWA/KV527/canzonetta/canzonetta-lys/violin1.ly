% Violin I part.
\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

Instrument = "Violin I"
\include "header.ly"
#(set-global-staff-size 20)
\include "notes.ly"

\score {
  \context Staff = ViolinI \ViolinIStaff

  \layout {}
  % no midi here, use score.ly for midi output.
}


