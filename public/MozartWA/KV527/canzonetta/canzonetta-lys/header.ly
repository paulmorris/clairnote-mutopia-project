\version "2.19.49"

\header {
 mutopiatitle = "Canzonetta \"Deh vieni alla finestra\" from Don Giovanni"
 mutopiacomposer = "MozartWA"
 mutopiapoet = "Lorenzo da Ponte"
 mutopiaopus = "KV 527"
 mutopiainstrument = "Ensemble: Baritone, Mandolin, Violin, Viola, 'Cello"
 instrument = \Instrument
 date = "1787/Oct"
 source = "Neue Mozartausgabe"
 style = "Classical"
 license = "Public Domain"
 maintainer = "Erik Sandberg"
 maintainerEmail = "ersa9195@student.uu.se"

 title = "Canzonetta \"Deh vieni alla finestra\""
 subtitle = "Don Giovanni, No. 16"
 composer = "W. A. Mozart (1756-1791)"
 opus = "KV 527"
 piece = "Allegretto"

 footer = "Mutopia-2013/01/06-189"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}
