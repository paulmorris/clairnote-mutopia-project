\version "2.19.49"

\header {
  title = "Trio in E-flat Major"
  subtitle = "(Kegelstatt Trio)"
  subsubtitle = "for Clarinet, Viola and Piano"
  composer = "W.A. Mozart (1756-1791)"
  opus = "KV.498"

  mutopiatitle = "Trio in E-flat Major KV. 498"
  mutopiacomposer = "MozartWA"
  mutopiaopus = "KV 498"
  mutopiainstrument = "Clarinet (or Violin), Viola and Piano (also Piano solo)"
  date = "1786"
  source = "Breitkopf und Haertel (1879)"
  style = "Classical"
  license = "Public Domain"
  maintainer = "Maurizio Tomasi"
  maintainerEmail = "zio_tom78@hotmail.com"
  moreInfo = "<p>A violin transposition of the clarinet part is included.</p>"

 footer = "Mutopia-2014/03/28-250"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

% Some useful macros

tupletNum = \revert TupletNumber.stencil

noTupletNum = \override TupletNumber.stencil = ##f

tupletBracket = \override TupletBracket.bracket-visibility = ##t

noTupletBracket = \override TupletBracket.bracket-visibility = ##f 

smallNatural=\markup {\tiny \natural}

markingsI =  {
    \tempo "Andante."
}

markingsII =  {
    \tempo "Menuetto."
    | s2.*41
    \tempo "Trio."
}

markingsIII =  {
    \tempo "Rondo. Allegretto."
}
