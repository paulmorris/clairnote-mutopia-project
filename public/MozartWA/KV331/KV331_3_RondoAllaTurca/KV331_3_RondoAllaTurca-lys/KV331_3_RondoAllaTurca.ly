\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\header {
  title = "Rondo Alla Turca"
  subtitle = "Turkish March"
  composer = "W. A. Mozart"
  piece = "Allegretto"
  mutopiatitle = "Sonate Opus KV 331 - Rondo Alla Turca"
  mutopiacomposer = "MozartWA"
  mutopiainstrument = "Piano"
  mutopiaopus = "KV 331"
  style = "Classical"
  license = "Public Domain"
  source = "IMSLP"

  maintainer = "Rune Zedeler and Chris Sawer"
  maintainerEmail = "chris@mutopiaproject.org"

 footer = "Mutopia-2015/08/13-108"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

\include "KV331_3_RondoAllaTurcaNotes.ly"

\score {
  \new PianoStaff
  <<
    \new Staff {
      \time 2/4

      \right
    }
    \new Dynamics {
      \override DynamicTextSpanner.style = #'none
      \dynamics
    }
    \new Staff {
      \left
    }
  >>
  \layout {}
  \midi {}
}
