\version "2.19.49"
\header {
                                % LILYPOND HEADERS
    head =  "0.05 (09 Jan 2015)"

    %% dedication = "Dedication"
    title = "Solace"
    subtitle = "A Mexican Serenade"
    %% subsubtitle = "Subsubtitle"

    composer = "Scott Joplin (1868-1905)"
    %% opus = "Opus"
    %% arranger = "Arranger"

    %% poet = "Poet"
    %% texttranslator = "Translator"
    %% meter = "meter"

    %% instrument = "Instrument"
    %% piece = "Piece"

                                % LILYPOND FOOTERS
    license = "Public Domain"
    %% tagline = "Tagline"


                                % MUTOPIA HEADERS
    mutopiatitle = "Solace"
    mutopiacomposer = "JoplinS"
    mutopiainstrument = "Piano"
    date = "1909"
    source = "Original Manuscript (1909)"
    style = "Jazz"
    enteredby = "Magnus Lewis-Smith"
    maintainer = "Magnus Lewis-Smith"
    maintainerEmail = "mlewissmith@users.sourceforge.net"
    maintainerWeb = "http://magware.sourceforge.net/"

 footer = "Mutopia-2015/01/25-482"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}




%{
BUGLIST
*	http:
*	category:  projects/lily
*	group:     sources/lily/joplin_solace


FEATURE REQUEST
*	http:
*	category:  projects/lily
*	group:     sources/lily/joplin_solace

Some interesting Scott Joplin links:
*    http:
*    http:
*    http:
*    http:
%}
