\version "2.19.49"

\score {
  \new PianoStaff <<
    \new Staff = "upper" { \globalT \RH }
    \new Staff = "lower" { \globalB \LH }
  >>
  \midi { \tempo 4 = 90 }
}