\version "2.19.49"

\score {
  \new PianoStaff <<
    \set PianoStaff.instrumentName = \lblExNum
    \new Staff = "upper" { \globalT \RH }
    \new Staff = "lower" { \globalB \LH }
  >>
  \layout { }
}