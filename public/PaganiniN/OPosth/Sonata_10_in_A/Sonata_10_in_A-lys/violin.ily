\version "2.19.49"

\new Staff = "Staff_violin" {
	\set Staff.instrumentName = "Violin"
	\set Staff.midiInstrument = "violin"
	\violin
}
