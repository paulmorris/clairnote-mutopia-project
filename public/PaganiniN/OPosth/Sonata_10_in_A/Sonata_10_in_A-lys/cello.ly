\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "header.ily"

\header {
	instrument = "Cello"
}

\layout {
	\compressFullBarRests
}

\include "notes.ily"

\score{
<<
	\include "cello.ily"
>>
	\midi {}
	\layout {}
}
