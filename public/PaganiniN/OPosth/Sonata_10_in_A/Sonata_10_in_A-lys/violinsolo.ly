\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "header.ily"

\header {
	instrument = "Violin Solo"
}

\layout {
	\compressFullBarRests
	\context { \Staff
		\override InstrumentName.stencil = ##f
	}
}

\include "notes.ily"

\score{
<<
	\include "violinsolo.ily"
>>
	\midi {}
	\layout {}
}
