\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "header.ily"

\header {
	instrument = "Violin"
}

\layout {
	\compressFullBarRests
}

\include "notes.ily"

\score{
<<
	\include "violin.ily"
>>
	\midi {}
	\layout {}
}
