\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "header.ily"

#(set-global-staff-size 18)

\include "notes.ily"

\score{
<<
	\include "violinsolo.ily"
	\include "violin.ily"
	\include "cello.ily"
>>
	\midi {}
	\layout {}
}
