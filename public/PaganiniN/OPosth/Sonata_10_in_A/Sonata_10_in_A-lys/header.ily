\version "2.19.49"

\header {
  title = "Sonata No. 10 in A"
  composer = "Nicolò Paganini"
  opus = "Op. Posth."
  style = "Romantic"
  license = "Public Domain"
  source = "B. Schott's Söhne, n. d. (ca. 1850). Plate 17530 (IMSLP #92136)"
  maintainer = "Felix Janda"
  maintainerEmail = "felix.janda (at) posteo.de"
  mutopiacomposer = "PaganiniN"
  mutopiainstrument = "Violin, Cello"

 footer = "Mutopia-2014/04/21-1950"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}
