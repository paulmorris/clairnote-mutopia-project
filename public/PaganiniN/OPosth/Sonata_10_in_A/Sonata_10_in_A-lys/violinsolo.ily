\version "2.19.49"

\new Staff = "Staff_violinsolo" {
	\set Staff.instrumentName = "Violin Solo"
	\set Staff.midiInstrument = "violin"
	\violinsolo
}
