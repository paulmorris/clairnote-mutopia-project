\version "2.19.49"

\new Staff = "Staff_cello" {
	\set Staff.instrumentName = "Cello"
	\set Staff.midiInstrument = "cello"
	\cello
}
