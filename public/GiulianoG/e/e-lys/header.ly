\version "2.19.49"

\header {
  title = "Sonata"
  subtitle = "Per Mandolino, e Basso"
  mutopiatitle = "Sonata in E Per Mandolino, e Basso"
  composer = "Giuseppe Giuliano (17??-?)"
  mutopiacomposer = "GiulianoG"
  mutopiaopus = ""
  mutopiainstrument = "Mandolin and Bass"
  date = "18th C."
  source = "Facsimile from The Music Library of Sweden"
  style = "Baroque"
  maintainer = "Erik Sandberg"
  maintainerEmail = "ersa9195@student.uu.se"

 license = "Public Domain"
 footer = "Mutopia-2014/03/24-362"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

