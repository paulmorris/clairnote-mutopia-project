\version "2.19.49"

\header {
 mutopiatitle = "Canon per 3 Violini e Basso"
 mutopiacomposer = "PachelbelJ"
 mutopiainstrument = "3 Violins, Cello"
 date = "1694"
 source = "IMSLP"
 style = "Baroque"
 license = "Creative Commons Attribution 4.0"
 maintainer = "Michael Fischer v. Mollard"
 maintainerEmail = "konfusator@gmail.com"
 title = " Canon per 3 Violini e Basso"
 composer = "Johann Pachelbel (1653-1706)"

 footer = "Mutopia-2015/09/02-2047"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Licensed under \with-url #"http://creativecommons.org/licenses/by/4.0/" {Creative Commons Attribution 4.0.} Free to distribute, modify, and perform.}}
 tagline = ##f
}

\layout {
  indent = 20.0 \mm
  short-indent = 10.0 \mm
  \pointAndClickOff
}
