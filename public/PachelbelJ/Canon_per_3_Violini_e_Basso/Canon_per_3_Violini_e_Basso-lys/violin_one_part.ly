\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

#(set-global-staff-size 20)

\include "header.ily"
\include "violin_one.ily"

\score {
  << 
  \compressFullBarRests
    \new Staff = violinI \with {
      midiInstrument = #"violin"
      instrumentName = \markup {
        \center-column { "Violin I" }
      }
    }
    \violinone
  >>
\include "paper.ily"
\include "midi.ily"
}
