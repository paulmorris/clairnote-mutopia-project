\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

#(set-global-staff-size 20)

\include "header.ily"
\include "violin_two.ily"

\score {
  <<
    \compressFullBarRests
    \new Staff = violinII \with {
      midiInstrument = #"violin"
      instrumentName = \markup {
        \center-column {"Violin II" }
      }
    }
    \violintwo
  >>
\include "paper.ily"
\include "midi.ily"
}
