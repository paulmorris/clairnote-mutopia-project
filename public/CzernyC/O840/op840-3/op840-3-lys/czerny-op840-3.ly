\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\language "deutsch"

\include "stile.ily"
\include "defn.ily"

exercise = "3"
tonality_deutsch = "C Moll"
tonality_fr = "Ut mineur."
meter = { \tempo 4 = 78 }

\header {
  composer            =       \markup { \bold "Carl Czerny" " (* 21. Februar 1791; † 15. Juli 1857)" }
  mutopiacomposer     =       "CzernyC"

  title               =       "50 Melodische Übungsstücke"
  mutopiatitle        =       "50 Melodische Übungsstücke, No. 3"

  opus                =       "Op. 840"
  piece               =       \markup { "Etüde" \number \tiny \exercise }
  mutopiaopus         =       "Op. 840, No. 3"

  source              =       "IMSLP; Mainz: Schott, n.d.[1855]. Plate 13253"
  style               =       "Romantic"
  moreInfo	      =       "Exercices Progressifs dans tous les tons majeurs et mineurs; Melodic studies"
  license             =       "Public Domain"
  enteredby           =       "Manuela" %Manuela Gößnitzer
  maintainer          =       "Manuela"
  maintainerEmail     =       "pressephotografin--gmail.com"
  maintainerWeb       =       "https://github.com/CarlCzerny/Op-840"
  mutopiainstrument   =       "Piano"
  
 footer = "Mutopia-2016/11/16-2148"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

\include "etude-03.ily"

\include "score-layout.ily"
\include "score-midi.ily"
