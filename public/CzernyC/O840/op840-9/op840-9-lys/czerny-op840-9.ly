\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\language "deutsch"

\include "stile.ily"
\include "defn.ily"

exercise = "9"
tonality_deutsch = "B dur"
tonality_fr =  \markup { "Si" \tiny \raise #0.4 \flat " majeur." }
meter = { \tempo 4 = 121 }


\header {
  composer            =       \markup { \bold "Carl Czerny" " (* 21. Februar 1791; † 15. Juli 1857)" }
  mutopiacomposer     =       "CzernyC"

  title               =       "50 Melodische Übungsstücke"
  mutopiatitle        =       "50 Melodische Übungsstücke, No. 9"

  opus                =       "Op. 840"
  piece               =       \markup { "Etüde" \number \tiny "9" }
  mutopiaopus         =       "Op. 840, No. 9"

  source              =       "IMSLP; Mainz: Schott, n.d.[1855]. Plate 13253"
  style               =       "Romantic"
  moreInfo	      =       "Exercices Progressifs dans tous les tons majeurs et mineurs; Melodic studies"
  license             =       "Public Domain"
  enteredby           =       "Manuela" %Manuela Gößnitzer
  maintainer          =       "Manuela"
  maintainerEmail     =       "pressephotografin--gmail.com"
  maintainerWeb       =       "https://github.com/CarlCzerny/Op-840"
  mutopiainstrument   =       "Piano"
  
 footer = "Mutopia-2016/12/05-2154"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

\paper {
    ragged-last-bottom = ##t
    ragged-right = ##f
}

\include "etude-09.ily"

\include "score-layout.ily"
\include "score-midi.ily"
