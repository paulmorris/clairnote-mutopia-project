\version "2.19.49"

\score {
  <<
    \new Staff="Discant" \RH
    \new Staff="Bass" \LH
  >>
  \midi { \meter }
}
