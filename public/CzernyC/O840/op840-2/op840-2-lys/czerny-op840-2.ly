\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\language "deutsch"

\include "stile.ily"
\include "defn.ily"

exercise = "2"
tonality_deutsch = "F Dur"
tonality_fr = "Fa majeur."
meter = { \tempo 4 = 118 }

\header {
  composer            =       \markup { \bold "Carl Czerny" " (* 21. Februar 1791; † 15. Juli 1857)" }
  mutopiacomposer     =       "CzernyC"

  title               =       "50 Melodische Übungsstücke"
  mutopiatitle        =       "50 Melodische Übungsstücke, No. 2"

  opus                =       "Op. 840"
  piece               =       \markup { "Etüde" \number \tiny \exercise }
  mutopiaopus         =       "Op. 840, No. 2"

  source              =       "IMSLP; Mainz: Schott, n.d.[1855]. Plate 13253"
  style               =       "Romantic"
  moreInfo	      =       "Exercices Progressifs dans tous les tons majeurs et mineurs; Melodic studies"
  license             =       "Public Domain"
  enteredby           =       "Manuela" %Manuela Gößnitzer
  maintainer          =       "Manuela"
  maintainerEmail     =       "pressephotografin--gmail.com"
  maintainerWeb       =       "https://github.com/CarlCzerny/Op-840"
  mutopiainstrument   =       "Piano"
  
 footer = "Mutopia-2016/11/15-2147"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}


\layout {
  \context {
    \Score
    \override NonMusicalPaperColumn.line-break-permission = ##f
    \override NonMusicalPaperColumn.page-break-permission = ##f
  }
}

\include "etude-02.ily"

\include "score-layout.ily"
\include "score-midi.ily"
