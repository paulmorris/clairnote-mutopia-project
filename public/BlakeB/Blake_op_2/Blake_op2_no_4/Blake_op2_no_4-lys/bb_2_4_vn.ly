\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "bb_2_4_header.ly"
\include "bb_2_4_parts.ly"

\score {
  \new StaffGroup \keepWithTag #'vn \part_duetto_iv
  \header {
    piece = "Duetto IV"
  }
  \layout { }
}
\pageBreak
\score {
  \new StaffGroup \keepWithTag #'vn \part_rondo
  \header {
    piece = "Rondo"
  }
  \layout { }
}
