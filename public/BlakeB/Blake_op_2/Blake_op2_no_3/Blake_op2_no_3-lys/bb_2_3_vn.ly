\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "bb_2_3_header.ly"
\include "bb_2_3_parts.ly"

\score {
  \new StaffGroup \keepWithTag #'vn \part_duetto_iii
  \header {
    piece = "Duetto III"
  }
  \layout { }
}
\pageBreak
\score {
  \new StaffGroup \keepWithTag #'vn \part_rondo
  \header {
    piece = "Rondo"
  }
  \layout { }
}
