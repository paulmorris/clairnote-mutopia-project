\version "2.19.49"
\header {
  title = "A Second Sett of Six Duetts for a Violin & Tenor"
  opus = "Op. 2 No. 6"
  composer = "Benjamin Blake"
  mutopiatitle = "A Second Sett of Six Duetts for a Violin & Tenor: No. 6"
  mutopiacomposer = "BlakeB"
  mutopiainstrument = "Violin, Viola"
  source = "London: J. Blundell, [1781]"
  date = "1781"
  style = "Baroque"
  license = "Creative Commons Attribution-ShareAlike 3.0"
  maintainer = "Huw Richards"
  maintainerEmail = "ahr (at) eng.cam.ac.uk"

 footer = "Mutopia-2013/08/18-1860"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Licensed under \with-url #"http://creativecommons.org/licenses/by-sa/3.0/" {Creative Commons Attribution-ShareAlike 3.0} Free to distribute, modify, and perform.}}
 tagline = ##f
}

\paper {
  markup-system-spacing.padding = #4
  top-margin = 12
  bottom-margin = 12
}

% Since Blake didn't use hairpins, force text dynamics
\layout {
  \context Voice {
    \override DynamicTextSpanner.style = #'none
    \crescTextCresc
    \dimTextDecresc
    \dimTextDim
  }
}
