\version "2.19.49"

\header {
  title = "Concerto di mandolino a solo con Violini e Basso"
  mutopiatitle = "Concerto di mandolino a solo con Violini e Basso (Gimo 58)"
  composer = "Domenico Caudioso (17??-?)"
  mutopiacomposer = "CaudiosoD"
  instrument = \Instrument
  mutopiainstrument = "Ensemble: Mandolin, 2 Violins, 'Cello"
  date = "c.1760"
  mutopiadate = "c. 1760"
  source = "Gimo Music Collection"
  style = "Baroque"
  license = "Public Domain"
  maintainer = "Erik Sandberg"
  maintainerEmail = "ersa9195@student.uu.se"

 footer = "Mutopia-2013/04/10-322"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}
