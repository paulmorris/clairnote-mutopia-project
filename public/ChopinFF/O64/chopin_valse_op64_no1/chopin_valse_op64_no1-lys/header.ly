\version "2.19.49"

\header {
                                % LILYPOND HEADERS       
    dedication = "A Madame la Comtesse DELPHINE POTOCKA"
    title = "Valse"
    subtitle = "`Minute Waltz'"
    %% subsubtitle = "subsubtitle"

    composer = "Frederic Chopin (1810-1849)"
    opus = "Op.~64, No.~1"
    %% arranger = "Arranger"

    %% poet = "Poet"
    %% texttranslator = "Translator"
    %% meter = "meter"

    %% instrument = "Pianoforte"
    %% piece = "\\textbf{\large Molto Vivace}"

                                % LILYPOND FOOTERS
    license = "Public Domain"
    %footer = "0.04 (12 Aug 2004)" 
    %% tagline = "tagline"

                                % MUTOPIA HEADERS       
    mutopiatitle = "Valse Op. 64, No. 1 ('Minute Waltz')"
    mutopiacomposer = "ChopinFF"
    mutopiaopus = "Op. 64"
    mutopiainstrument = "Piano"
    date = "1847"
    source = "Edition Peters"
    style = "Romantic"
    enteredby = "Magnus Lewis-Smith"
    maintainer = "Magnus Lewis-Smith"
    maintainerEmail = "mlewissmith@users.sourceforge.net"
    maintainerWeb = "http://magware.sourceforge.net/"

 footer = "Mutopia-2015/01/17-483"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}




%{
BUGLIST
*	http: 
*	category:  projects/lily
*	group:     sources/lily/chopin_valse_op64_no1

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

FEATURE REQUEST
*	http: 
*	category:  projects/lily
*	group:     sources/lily/chopin_valse_op64_no1

LINKS
*	http: 
*	http: 
*	http: 
*	http: 

%}
