\version "2.19.49"
\header {
                                % LILYPOND HEADERS
    head =  "0.05 (08 Jan 2015)"

    title = "Prelude"
    subtitle = "`The Polish Dance'"

    composer = "Frederic Chopin (1810-1849)"
    opus = "Op. 28, No. 7"

    %%    poet = "Poet"
    %%    texttranslator = "Translator"
    %%    meter = "meter"

    %%    instrument = "Instrument"
    %%    piece = "Piece"

                                % LILYPOND FOOTERS
    license = "Public Domain"
    %footer = "0.05 (12 Aug 2004)"
    %%    tagline = "Tagline"

                                % MUTOPIA HEADERS
    mutopiatitle = "Prelude: Op. 28, No. 7"
    mutopiacomposer = "ChopinFF"
    mutopiaopus = "Op. 28"
    mutopiainstrument = "Piano"
    date = "1838/39"
    source = "Edition Peters"
    style = "Romantic"
    enteredby = "Magnus Lewis-Smith"
    maintainer = "Magnus Lewis-Smith"
    maintainerEmail = "mlewissmith@users.sourceforge.net"
    maintainerWeb = "http://magware.sourceforge.net/"

 footer = "Mutopia-2015/01/17-470"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}


%{
BUGLIST
*	http:
*	category:  projects/lily
*	group:     sources/lily/chopin_prelude_op28_no7


FEATURE REQUEST
*	http:
*	category:  projects/lily
*	group:     sources/lily/chopin_prelude_op28_no7

LINKS
*	http:
%}
