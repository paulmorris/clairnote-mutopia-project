\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
#(set-global-staff-size 20)
\header {
  title             = "Fuga"
  subtitle          = "from the Aylesford Pieces"
  composer          = "Georg Friedrich Händel (1685-1759)"
  meter             = "Moderato"
  mutopiatitle      = "Fuga"
  mutopiacomposer   = "HandelGF"
  mutopiainstrument = "Harpsichord, Piano"
  date              = "18th Century"
  source            = "Edition Schott 1930"
  style             = "Baroque"
  license = "Public Domain"
  maintainer        = "Bas Wassink"
  maintainerEmail   = "basvanlola@hotmail.com"

 footer = "Mutopia-2013/02/21-153"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
  }

Global =  {\key g\major \time 4/4}


MDI =  \relative c'' {
  g4. b8 a d, d' c |
  b g g'2 fis4 |
  g8 d e b cis a d fis, |
  g b a g fis a d4 ~ |
  
  d4 c!2 b4 |
  a2 g4 g' ~ |
  g f2 e4 |
  <<{d2 c4.} \\ {a4 b e,4.}>> e'8 |
  
  d8 g, g' f e d c4 ~
  c b a d ~
  d c b4. e8
  cis a d2 cis4
  
  d a8 b c! e d c
  b d g fis e d c b
  a4 d2 c4 ~
  c b a2
  
  g4 g'8 fis e d c b
  a fis' g b, <<{a4. g8} \\ {d2} \\ \\ {g4 fis}>>
  <<{g1\fermata} \\ {<d b>}>>
  \bar "|." 
  }

MSI =  \relative c {
  g4-. g'2 fis4
  g4. b8 a d, d' c
  b4 g2 fis8 b
  e,4 cis' d4. d,8
  
  e d e fis g d g b,
  c a d c b g r e'
  a g a b c g a e
  f d g g, c4 c' ~
  
  c b c r8 e,
  fis d g2 fis8 b
  gis e a2 g4 ~
  g fis e8 fis16 g a8 g
  
  fis d fis g a4 fis
  g b c8 b a g
  fis e fis d e d e fis
  g d e b c a d c
  
  b a g4 c8 b a4
  d8 c b c d4 d,
  g1\fermata
  }

\score { {
  \new PianoStaff <<
    \set PianoStaff.midiInstrument = "harpsichord"
    \new Staff = "up" <<
      \Global \clef treble \MDI
    >>
    \new Staff = "down" <<
      \Global \clef bass \MSI
    >>
  >>
}

  \midi {
    \tempo 4 = 88
    }


\layout {}
}
