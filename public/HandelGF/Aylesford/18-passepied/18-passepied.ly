\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
#(set-global-staff-size 20)
\header {
  title             = "Passepied (et Menuet)"
  subtitle          = "from the Aylesford Pieces"
  composer          = "Georg Friedrich Händel (1685-1759)"
  meter             = "Andantino"
  mutopiatitle      = "Passepied (et Menuet)"
  mutopiacomposer   = "HandelGF"
  mutopiainstrument = "Harpsichord, Piano"
  date              = "18th Century"
  source            = "Edition Schott 1930"
  style             = "Baroque"
  license = "Public Domain"
  maintainer        = "Bas Wassink"
  maintainerEmail   = "basvanlola@hotmail.com"

 footer = "Mutopia-2013/02/21-167"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
  }

Global =  {\key a\major \time 3/8 \partial 8}


MDI =  \relative c'' {
  \repeat volta 2 {
  \oneVoice e8-.
  \voiceOne d16 cis b a fis'8-.
  e4 d8
  cis b16 a b8-.
  e,4 a8-.
  gis16 a b gis a8-.
  b16 cis d b cis8-.
  fis8 e \tuplet 5/4 { dis32[ e dis cis dis]}
  e4 
  }
  
  \repeat volta 2 {
  \oneVoice b8-.
  a16 gis fis e e'8-.
  \voiceOne d4 e8-.
  cis b16 a cis8-.
  b4 cis8
  d16 e fis d e8-.
  fis16 gis a fis gis8-.
  a-. cis, b
  a4\fermata
  }
  }
MDII =  \relative c'' {
  s8
  s4 a8
  a4 gis8
  a4 fis8
  s4 e8
  d4 e8
  e4 e8
  a4 fis8
  gis4
  
  s8
  s4.
  a4 b8
  e,4 e8
  gis4 ais8
  b4 a!8
  a4 d8
  e a, gis
  cis,4
  }

MSI =  \relative c' {
  <<{
    e8-. |
    e4
  } \\ {
    gis,8 |
    a4
  }>> d8-. |
  cis8 b16 a b8-.
  a4 d,8
  <<gis4. \\ {e8 d cis-.}>>
  b4 cis8
  gis4 a8
  fis4 b8
  e4
  
  <<{
    b'8-. |
    b4 cis8-. |
  } \\ {
    dis,8 |
    e4 a8 |
  }>>
  fis e16 d! gis8-.
  a4 a,8-.
  e' d cis-.
  b4 cis8
  d4 b8
  cis-. d-. e-.
  a,4\fermata
  }

\score { {
  \new PianoStaff <<
    \set PianoStaff.midiInstrument = "harpsichord"
    \new Staff = "up" <<
      \Global \clef treble
      \new Voice=One {\voiceOne\MDI}
      \new Voice=Two {\voiceTwo\MDII}
    >>
    \new Staff = "down" <<
      \Global \clef bass \MSI
    >>
  >>
}

  \midi {
    \tempo 4 = 80
    }


\layout {}
}
