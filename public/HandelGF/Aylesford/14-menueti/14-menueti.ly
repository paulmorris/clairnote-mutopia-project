\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
#(set-global-staff-size 20)
\header {
  title             = "Menuet I"
  subtitle          = "from the Aylesford Pieces"
  composer          = "Georg Friedrich Händel (1685-1759)"
  meter             = "Allegretto"
  mutopiatitle      = "Menuet I"
  mutopiacomposer   = "HandelGF"
  mutopiainstrument = "Harpsichord, Piano"
  date              = "18th Century"
  source            = "Edition Schott 1930"
  style             = "Baroque"
  license = "Public Domain"
  maintainer        = "Bas Wassink"
  maintainerEmail   = "basvanlola@hotmail.com"

 footer = "Mutopia-2013/02/21-163"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
  }

Global =  {\key f\major \time 3/8 \partial 8}


MDI =  \relative c'' {
  \repeat volta 2 {
  c8
  
  a16. bes32 c8 c-.
  a16. bes32 c8 c-.
  d f-. e-.
  f8 e16 d c8
  
  d-. c16 b a g
  g'8 f16 e d c
  c8 e16 d c d
  c4
  }
  
  c8 |
  
  f-. f16 e f d
  g8-. c, bes
  a8-. bes16 a g f
  c'8-. f16 e f8
  
  d-. c16 bes a g
  f e f8 bes-.
  a16 g g8. f16
  f4
  \bar "||"
  
  c'8 |
  
  f-. f16 e f d
  g8-. c, bes
  a8-. bes16 a g f
  c'8-. f16 e f8
  
  d-. c16 bes a g
  f e f8 bes-.
  a16 g g8. f16
  f4.\fermata
  \bar "|."
  }
MDII =  \relative c'' {
  a8
  
  f a a-.
  f a a-.
  f bes-. g-.
  c a g
  
  a f4
  c'8 g e
  a g d
  e4
  
  e8
  
  f-. g-. a-.
  <g c> g4
  f8 c4
  e8-. d'-. c-.
  
  f,-. d-. e-.
  d-. bes-. c-.
  f8 f e
  a,4
  
  e'8-.
  
  f-. g-. a-.
  <g c> g4
  f8 c4
  e8-. d'-. c-.
  
  f,-. d-. e-.
  d-. bes-. c-.
  f8 f e
  a,4.
  
  }

MSI =  \relative c {
  f8
  
  f, f' f-.
  f, f' f-.
  bes g-. c-.
  a-. f-. e-.
  
  f-. d-. g-.
  e-. g-. a-.
  f-. g-. g,-.
   c[-. c'-.]
  
  bes8-.
  
  a-. g-. f-.
  e-. c16 d e c
  f8-. e-. d-.
  <c c'>-. <bes bes'>-. <a a'>-.
  
  <bes bes'> bes-. c-.
  d-. d-. e-.
  f-. c-. c,-.
  f4
  
  <bes bes'>8-.
  
  <a a'>-. <g g'>-. <f f'>-.
  <e e'>-. c'16 d e c
  f8-. e-. d-.
  c-. <bes bes'>-. <a a'>-.
  
  <bes bes'>-. <bes, bes'>-. <c c'>-.
  <d d'>-. <d d'>-. <e e'>-.
  <f f'>-. <c c'>-. c-.
  <f,\fermata f'>4. 
  }

\score { {
  \new PianoStaff <<
    \set PianoStaff.midiInstrument = "harpsichord"
    \new Staff = "up" <<
      \Global \clef treble
      \new Voice=One {\voiceOne\MDI}
      \new Voice=Two {\voiceTwo\MDII}
    >>
    \new Staff = "down" <<
      \Global \clef bass \MSI
    >>
  >>
}

  \midi {
    \tempo 4 = 108
    }


\layout {}
}
