\version "2.19.49"

\header {
%  title = "Concerto con Viola d'amore e Leuto e con tutti gl'Istromenti sordini"
%  mutopiatitle = "Concerto con Viola d'amore e Leuto e con tutti gl'Istromenti sordini"
  mutopiatitle = "Concerto in D Minor for Viola d'amore and Lute"
  title = "Concerto in D Minor"
  subtitle = "for Viola d'amore and Lute"
  composer = "Antonio Vivaldi (1678-1741)"
  mutopiacomposer = "VivaldiA"
  instrument = \Instrument
  mutopiainstrument = "Viola d'amore, Lute, String Ensemble: Violins, Viola, Cello, Basso Continuo"
  date = "18th C."
  mutopiadate = "18th Century"
  source = "Ricordi"
  style = "Baroque"
  license = "Public Domain"
  maintainer = "Erik Sandberg"
  maintainerEmail = "ersa9195@student.uu.se"

 footer = "Mutopia-2013/02/17-310"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}
