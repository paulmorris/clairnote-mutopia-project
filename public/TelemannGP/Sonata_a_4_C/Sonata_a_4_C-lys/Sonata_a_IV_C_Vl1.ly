\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\language "deutsch"
\include "Sonata_a_IV_C.ly"

\paper {
  ragged-last-bottom = ##f
}

\header {
  title = "Sonata â IV. Violini"
  subtitle = "Violin I"
  composer = "Georg Philipp Telemann"
  mutopiatitle = "Sonata in C major for four violins"
  mutopiacomposer = "TelemannGP"
  mutopiaopus = "TWV 40:203"
  mutopiainstrument = "Violin"
  source = "Manuscript"
  style = "Baroque"
  license = "Creative Commons Attribution-ShareAlike 4.0"
  maintainer = "Urs Metzger"
  maintainerEmail = "urs@ursmetzger.de"

 footer = "Mutopia-2016/04/22-2115"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Licensed under \with-url #"http://creativecommons.org/licenses/by-sa/4.0/" {Creative Commons Attribution-ShareAlike 4.0} Free to distribute, modify, and perform.}}
 tagline = ##f
}

\score {
  \header {
    opus = "TWV 40:203"
    piece = \markup { \bold \huge "Grave" }
  }
  \graveAViolinI
  \layout {
    indent = 0.6 \cm
  }
}

\score {
  \header {
    piece = \markup { \bold \huge "Allegro" }
  }
  \allegroAViolinI
  \layout {
    indent = 0.6 \cm
  }
}

\pageBreak

\score {
  \header {
    piece = \markup { \bold \huge "Largo e Staccato" }
  }
  \largoAViolinI
  \layout {
    indent = 0.6 \cm
  }
}

\score {
  \header {
    piece = \markup { \bold \huge "Allegro" }
  }
  \allegroIIAViolinI
  \layout {
    indent = 0.6 \cm
  }
}
