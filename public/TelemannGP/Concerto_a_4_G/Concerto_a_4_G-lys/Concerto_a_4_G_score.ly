\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\language "deutsch"
\include "Concerto_a_4_G.ly"

\paper {
  ragged-last-bottom = ##f
}

\header {
  title = \markup {
    \column \center-align {
      \fontsize #6 \caps "Concerto"
      \fontsize #-2 "à"
      \fontsize #0 "4 Violini Concertati"
      \fontsize #-2 "del Sigr:"
      \vspace #0.6
      \fontsize #6 \caps "Melante"
    }
  }
  composer = "Georg Philipp Telemann"
  mutopiatitle = "Concerto in G major for four violins"
  mutopiacomposer = "TelemannGP"
  mutopiaopus = "TWV 40:201"
  mutopiainstrument = "Violin"
  source = "Manuscript"
  style = "Baroque"
  license = "Creative Commons Attribution-ShareAlike 4.0"
  maintainer = "Urs Metzger"
  maintainerEmail = "urs@ursmetzger.de"

 footer = "Mutopia-2016/05/18-2117"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Licensed under \with-url #"http://creativecommons.org/licenses/by-sa/4.0/" {Creative Commons Attribution-ShareAlike 4.0} Free to distribute, modify, and perform.}}
 tagline = ##f
}

largoViolinIPart = \new Staff \with {
  instrumentName = "I"
  midiInstrument = "violin"
  midiPanPosition = #-1
} \largoViolinI

largoViolinIIPart = \new Staff \with {
  instrumentName = "II"
  midiInstrument = "violin"
  midiPanPosition = #-0.33
} \largoViolinII

largoViolinIIIPart = \new Staff \with {
  instrumentName = "III"
  midiInstrument = "violin"
  midiPanPosition = #0.33
} \largoViolinIII

largoViolinIVPart = \new Staff \with {
  instrumentName = "IV"
  midiInstrument = "violin"
  midiPanPosition = #1
} \largoViolinIV

\score {
  \header {
    opus = "TWV 40:201"
    piece = \markup { \bold \huge "Largo e staccato" }
  }
  <<
    \largoViolinIPart
    \largoViolinIIPart
    \largoViolinIIIPart
    \largoViolinIVPart
  >>
  \layout {
    indent = 6
  }
  \midi { }
}

\pageBreak

allegroViolinIPart = \new Staff \with {
  instrumentName = "I"
  midiInstrument = "violin"
  midiPanPosition = #-1
} \allegroViolinI

allegroViolinIIPart = \new Staff \with {
  instrumentName = "II"
  midiInstrument = "violin"
  midiPanPosition = #-0.33
} \allegroViolinII

allegroViolinIIIPart = \new Staff \with {
  instrumentName = "III"
  midiInstrument = "violin"
  midiPanPosition = #0.33
} \allegroViolinIII

allegroViolinIVPart = \new Staff \with {
  instrumentName = "IV"
  midiInstrument = "violin"
  midiPanPosition = #1
} \allegroViolinIV

\score {
  \header {
    piece = \markup { \bold \huge "Allegro" }
  }
  <<
    \allegroViolinIPart
    \allegroViolinIIPart
    \allegroViolinIIIPart
    \allegroViolinIVPart
  >>
  \layout {
    indent = 6
  }
  \midi { }
}

adagioViolinIPart = \new Staff \with {
  instrumentName = "I"
  midiInstrument = "violin"
  midiPanPosition = #-1
} \adagioViolinI

adagioViolinIIPart = \new Staff \with {
  instrumentName = "II"
  midiInstrument = "violin"
  midiPanPosition = #-0.33
} \adagioViolinII

adagioViolinIIIPart = \new Staff \with {
  instrumentName = "III"
  midiInstrument = "violin"
  midiPanPosition = #0.33
} \adagioViolinIII

adagioViolinIVPart = \new Staff \with {
  instrumentName = "IV"
  midiInstrument = "violin"
  midiPanPosition = #1
} \adagioViolinIV

\score {
  \header {
    piece = \markup { \bold \huge "Adagio" }
  }
  <<
    \adagioViolinIPart
    \adagioViolinIIPart
    \adagioViolinIIIPart
    \adagioViolinIVPart
  >>
  \layout {
    indent = 6
  }
  \midi { }
}

\pageBreak

vivaceViolinIPart = \new Staff \with {
  instrumentName = "I"
  midiInstrument = "violin"
  midiPanPosition = #-1
} \vivaceViolinI

vivaceViolinIIPart = \new Staff \with {
  instrumentName = "II"
  midiInstrument = "violin"
  midiPanPosition = #-0.33
} \vivaceViolinII

vivaceViolinIIIPart = \new Staff \with {
  instrumentName = "III"
  midiInstrument = "violin"
  midiPanPosition = #0.33
} \vivaceViolinIII

vivaceViolinIVPart = \new Staff \with {
  instrumentName = "IV"
  midiInstrument = "violin"
  midiPanPosition = #1
} \vivaceViolinIV

\score {
  \header {
    piece = \markup { \bold \huge "Vivace" }
  }
  <<
    \vivaceViolinIPart
    \vivaceViolinIIPart
    \vivaceViolinIIIPart
    \vivaceViolinIVPart
  >>
  \layout {
    indent = 6
  }
  \midi { }
}
