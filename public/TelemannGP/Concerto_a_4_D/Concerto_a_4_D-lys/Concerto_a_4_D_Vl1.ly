\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\language "deutsch"
\include "Concerto_a_4_D.ly"

\paper {
  ragged-last-bottom = ##f
  ragged-right = ##f
}

\header {
  title = "Concerto à 4 Violini Concertati"
  subtitle = "Violin I"
  composer = "Georg Philipp Telemann"
  mutopiatitle = "Concerto in D major for four violins"
  mutopiacomposer = "TelemannGP"
  mutopiaopus = "TWV 40:202"
  mutopiainstrument = "Violin"
  source = "Manuscript"
  style = "Baroque"
  license = "Creative Commons Attribution-ShareAlike 4.0"
  maintainer = "Urs Metzger"
  maintainerEmail = "urs@ursmetzger.de"

 footer = "Mutopia-2016/05/18-2118"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Licensed under \with-url #"http://creativecommons.org/licenses/by-sa/4.0/" {Creative Commons Attribution-ShareAlike 4.0} Free to distribute, modify, and perform.}}
 tagline = ##f
}

\score {
  \header {
    opus = "TWV 40:202"
    piece = \markup { \bold \huge "Adagio" }
  }
  \adagioViolinI
  \layout {
    indent = 6
  }
}

\score {
  \header {
    piece = \markup { \bold \huge "Allegro" }
  }
  \allegroViolinI
  \layout {
    indent = 6
  }
}

\pageBreak

\score {
  \header {
    piece = \markup { \bold \huge "Grave" }
  }
  \graveViolinI
  \layout {
    indent = 6
  }
}

\score {
  \header {
    piece = \markup { \bold \huge "Allegro" }
  }
  \allegroIIViolinI
  \layout {
    indent = 6
  }
}
