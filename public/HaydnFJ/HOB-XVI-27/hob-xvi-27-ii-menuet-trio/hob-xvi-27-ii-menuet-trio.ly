\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\include "nederlands.ly"
#(set-global-staff-size 16)
\header {
  title             = "Sonata I"
  subtitle          = "6 Sonate 1776"
  composer          = "Franz Joseph Haydn (1732-1809)"
  opus              = "Hob. XVI:27"
  meter             = "II - Menuetto"
  license = "Public Domain"
  mutopiatitle      = "Sonate I, II - Menuetto"
  mutopiacomposer   = "HaydnFJ"
  mutopiainstrument = "Harpsichord, Piano"
  date              = "18th Century"
  source            = "C. F. Peters, 1880s"
  style             = "Classical"
  license = "Public Domain"
  maintainer        = "Bas Wassink"
  maintainerEmail   = "basvanlola@zonnet.com"

 footer = "Mutopia-2013/07/07-186"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

Global =  {\key g\major \time 3/4}
Treble = \clef treble
Bass = \clef bass
staffUp = \change Staff = "up"
staffDown = \change Staff = "down"
flat = \markup {\flat}
sharp = \markup {\sharp}

MD =  \relative c'' {
  \repeat volta 2 {
  d4^\sharp\turn \tuplet 3/2 { g8[ d b]} \tuplet 3/2 { d8[ b g]}
  \tuplet 3/2 { g8[ fis d]} <a'  c>4(  < g b>)
  \tuplet 3/2 { e8[ gis a]} \tuplet 3/2 { b[ c d]} \tuplet 3/2 { e[ c a]}
  g!2 ( fis4)

  d'4^\sharp\turn \tuplet 3/2 { g8[ d b]} \tuplet 3/2 { d[ b g]}
  \tuplet 3/2 { g[ fis d]} <a'  c>4(  < g b>)
  \tuplet 3/2 { c8[ b a]} <cis g'  >4( < d fis>)
  \tuplet 3/2 { b8[ e dis]} \tuplet 3/2 { fis[ e g]} \tuplet 3/2 { b[ g e]}

  \tuplet 3/2 { d![ cis e]} \tieUp a4 ~ \tuplet 3/2 { a8[ fis d]}
  \tuplet 3/2 { cis[ a e']} a4 ~ \tuplet 3/2 { a8[ fis d]}
  \tuplet 3/2 { d[ cis e]} a4 ~ \tuplet 3/2 { a8[ fis d]}
  \tuplet 3/2 { cis[ a e']} a4 ~ \tuplet 3/2 { a8[ fis d]}

  \tuplet 3/2 { cis[ b ais']} \tuplet 3/2 { b[ g e]} \tuplet 3/2 { g[ e cis]}
  cis2 ( d4)
  }

  \repeat volta 2 {
  d4^\sharp\turn \tuplet 3/2 { d'8[ b g]} \tuplet 3/2 { a[ fis d]}
  d2 ( cis4)\turn
  r \tuplet 3/2 { d8[ fis a]} \tuplet 3/2 { c![ a fis]}
  c4 ( b)\turn g'

  dis8 ( e) e4 g
  cis,8 ( d!) d4 \tuplet 3/2 { d'8[ b g]}
  fis4 ( e)\turn \tuplet 3/2 { g8[ fis e]}
  e4 ( d) \tuplet 3/2 { fis8[ g b]}

  \tuplet 3/2 { dis,[ e g]} \tuplet 3/2 { b,[ c e]} \tuplet 3/2 { gis,[ a c]}
  g!2 ( fis4)
  d'4^\sharp\turn \tuplet 3/2 { g8[ d b]} \tuplet 3/2 { d[ b g]}
  \tuplet 3/2 { g[ fis d]} <a'  c>4( < g b>)

  \tuplet 3/2 { e8[ gis a]} \tuplet 3/2 { b[ c d]} \tuplet 3/2 { e[ c a]}
  g!2 ( fis4)
  \staffDown \voiceOne \tuplet 3/2 { g,8[ fis d]} <fis c'  >4( <g b>)
  \staffUp \oneVoice \tuplet 3/2 { g'8[ fis d]} <fis c'  >4( <g b>)

  \voiceOne \tuplet 3/2 { g'8[ fis d]} <fis c'  >4( <g b>)
  \oneVoice \tuplet 3/2 { g,8[ fis d]} <c' e\fermata  >4( <b d\fermata>)
  \tuplet 3/2 { gis8[ a c]} \tuplet 3/2 { e[ c a]} \tuplet 3/2 { c[ a fis]}
  <c  fis>2( < b g'>4)

  \tuplet 3/2 { g''8[ fis a]} d4 ~ \tuplet 3/2 { d8[ b g]}
  \tuplet 3/2 { fis8[ d a']} d4 ~ \tuplet 3/2 { d8[ b g]}
  \tuplet 3/2 { g8[ fis a]} d4 ~ \tuplet 3/2 { d8[ b g]}
  \tuplet 3/2 { fis8[ d a']} d4 ~ \tuplet 3/2 { d8[ b g]}

  \tuplet 3/2 { e8[ fis g]} \tuplet 3/2 { d[ fis g]} \tuplet 3/2 { c,[ gis' a]}
  \tuplet 3/2 { b,[ b' a]} \tuplet 3/2 { g![ fis e]} \tuplet 3/2 { d[ c b]}
  \tuplet 3/2 { a[ c e]} g,4 fis\trill
  <c  fis>2( < b g'>4)
  }

  \repeat volta 2 {
  \key g\minor \mark \markup "Trio" \voiceOne d'2 \slashedGrace f8 es4
  d8 bes' ( a g fis  g)
  d2 \slashedGrace f8 es4
  d8 ( c' a fis d  c)

  \oneVoice bes4\trill ~ bes8. ( a32 bes  a8) r
  c4\trill ~ c8. ( bes32 a  bes8) r
  g ( g' f es d cis
   cis2) ( d4)
  }

  \repeat volta 2 {
  <d, f>2 \slashedGrace a'8 <es g>4
  <d f> r \slashedGrace f'8 <a, es>4
  <bes d> r \slashedGrace a'8 <es g>4
  <d f> r bes'8 ( a)

  a ( g) g ( f) f ( es)
  es2\turn \slashedGrace f8 es8 ( d)
  \slashedGrace es8 d8 ( c) \slashedGrace d8 c8 ( bes) \slashedGrace c8 bes8 ( a)
  a4 ( bes) r
  
  \voiceOne d2 \slashedGrace f8 es4
  d8 bes' ( a g fis  g)
  d2 \slashedGrace f8 es4
  d8 ( c' a fis d  c)

  \oneVoice bes4\trill ~ bes8. ( a32 bes  a8) r
  c4\trill ~ c8. ( bes32 a  bes8) r
  bes8\prall ( a) a\prall ( g) g\prall ( fis)
  fis4 ( g) r
  }
  
  }

MSI =  \relative c' {
  <g b>4 r <b d>
  <a c> fis ( g)
  c,2.
  d4 \tuplet 3/2 { d'8[ e d]} \tuplet 3/2 { c[ b a]}

  <g b>4 r <b d>
  <a c> fis ( g)
  r e' ( d)
  g2 r4

  r <cis, g'  >( < d fis>)
  r <cis g'  >( < d fis>)
  r <g, cis  >( < fis d'>)
  r <g cis  >( < fis d'>)

  g r <a g'>
  <<{g'2 ( fis4)} \\ {d2.}>>

  \Treble \tuplet 3/2 { d8[ fis a]} \tuplet 3/2 { d,8[ fis a]} \tuplet 3/2 { d,8[ fis a]}
  \tuplet 3/2 { e[ g a]} \tuplet 3/2 { e[ g a]} \tuplet 3/2 { e[ g a]}
  \tuplet 3/2 { fis[ a c]} r4 r
  \tuplet 3/2 { g,8[ d' g]} \tuplet 3/2 { g,[ d' g]} \tuplet 3/2 { g,[ d' g]}

  \tuplet 3/2 { c,[ e g]} \tuplet 3/2 { c,[ e g]} \tuplet 3/2 { c,[ e g]}
  \tuplet 3/2 { b,[ d g]} \tuplet 3/2 { b,[ d g]} \tuplet 3/2 { b,[ d g]}
  \tuplet 3/2 { c,[ e g]} \tuplet 3/2 { c,[ e g]} \tuplet 3/2 { c,[ e g]}
  \tuplet 3/2 { b,[ d g]} \tuplet 3/2 { b,[ d g]} \tuplet 3/2 { b,[ d g]}

  c,4 r r
  \Bass d, \tuplet 3/2 { d'8[ e d]} \tuplet 3/2 { c[ b a]}
  <g b>4 r <b d>
  <a c> fis ( g)

  c,2.
  d4 a d,
  \voiceTwo r a' ( g)
  \oneVoice r a' ( g)

  \staffUp \voiceTwo r4 a' ( g)
  \staffDown \oneVoice r4 fis,\fermata ( g)\fermata
  c,4 r d
  e2.

  r4 \Treble <fis'  c'>4( < g b>)
  r <fis  c'>( < g b>)
  r <c,  fis>( < b g'>)
  r <c  fis>( < b g'>)

  <<{g'2 fis4} \\ {c b a}>>
  <g g'> r r
  \Bass <c, c'> <d b'> <d a' c>
  g d g,

  \key g\minor <g' bes>4 \staffUp \voiceTwo <g'  bes>( < fis a>)
  <g bes> r r
  \staffDown \oneVoice <fis, a> \staffUp \voiceTwo <fis'  a>( < g bes>)
  <fis a> r r

  \staffDown \oneVoice r4 <g, d'> <fis d'>
  r <fis d'> <g d'>
  es r \Treble <es' g>
  <<{g2 ( fis4)} \\ d2.>>
  
  \Bass bes,8 bes' bes, bes' bes, bes'
  bes, bes' bes, bes' bes, bes'
  bes, bes' bes, bes' bes, bes'
  bes, bes' bes, bes' <d, bes'>4
  
  <<{bes'2.
  r4 f'2} \\
  {es,4 f g
  a2 bes4}>>
  <es, es'>4 <f d'> <f c'>
  bes4 d8 c bes a
  
  <g bes>4 \staffUp \voiceTwo <g'  bes>( < fis a>)
  <g bes> r r
  \staffDown \oneVoice <fis, a> \staffUp \voiceTwo <fis'  a>( < g bes>)
  <fis a> r r

  \staffDown \oneVoice r4 <g, d'> <fis d'>
  r <fis d'> <g d'>
  <c, c'> <d bes'> <d a'>
  g_ \markup{\italic "Men. d. C."} d g,
  }

\score { {
\new PianoStaff <<
  \set PianoStaff.midiInstrument = "harpsichord"
  \new Staff = "up" <<
    \Global \Treble
    \MD
  >>
  \new Staff = "down" <<
    \Global \Bass
    \MSI
    >>
>>
}
\layout {}

  \midi {
    \tempo 4 = 92
    }


}

