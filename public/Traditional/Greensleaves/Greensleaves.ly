\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\header {
    title = "Greensleaves"
    composer = "Traditional"
    mutopiatitle = "Greensleaves"
    mutopiacomposer = "Traditional"
    mutopiainstrument = "Lute, Guitar, Vihuela"
    mutopiasource = "Unknown"
    style = "Renaissance"
    license = "Public Domain"
    maintainer = "Aaron Fontaine"
    maintainerEmail = "afontain@d.umn.edu"

 footer = "Mutopia-2013/03/23-109"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}

#(set-global-staff-size 26)

melody = \new Voice \relative a' {
    \voiceOne
    a4 |
    c2 d4 e4. f8 e4 | d2 b4 g4. a8 b4 |
    c2 a4 a4. gis8 a4 | b2 gis4 e2 a4 |
    c2 d4 e4. f8 e4 | d2 b4 g4. a8 b4 |
    c4. b8 a4 gis4. fis8 gis4 | a2. a2.
    \bar "||"
    g'2. g4. f8 e4 | d2 b4 g4. a8 b4 |
    c2 a4 a4. gis8 a4 | b2 gis4 e2. |
    g'2. g4. f8 e4 | d2 b4 g4. a8 b4 |
    c4. b8 a4 gis4. fis8 gis4 | a2. a2. \bar "|."
}

harmony = \new Voice \relative a {
    \voiceTwo
    r4 |
    a2 b4 c2. | g2. b2. |
    a2. f'2. | e2. e,2. |
    a2 b4 c2. | g2. b2. |
    a2. e2. | a2. a2. |

    <c e>2. <c e>2. | g2. b2. |
    a2. f'2. | e2. e,2. |
    <c' e>2. <c e>2. | g2. b2. |
    a2. e2. | a2. a2. |
}

\score {
     {
        \new Staff <<
            \time 3/4
            \clef treble
            \key a \minor
            \partial 4

            \melody
            \harmony
        >>
    }

    \layout {
        indent = 0.0
        interscoreline = 1.5 \cm
    }


  \midi {
    \tempo 4 = 160
    }


}
