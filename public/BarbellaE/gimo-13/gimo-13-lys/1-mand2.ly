\version "2.19.49"

IMandII =  \relative c'' {
  \clef "treble"
  \key g \major
  \time 4/4
  \tupletSpan 4

  % end beams on quarters by default

  \repeat "volta" 2 {
  <b d, g,>8 <b d, g,> \tuplet 6/4 {r16  g[ a b c b]}
    <a fis d>8 <a fis d> \tuplet 6/4 {r16  fis[ g a b a]} |
  <g g,>8 <g g,> \tuplet 6/4 {r16  b[ a b b, d]} <a' e c>8[
    \tuplet 3/2 {a16 b c]} \tuplet 6/4 { b[ a g c, b a]} |
  %3
  g8 g r16 <a' d,> <b d,> <c d,> <d d, g,>8 <d d, g,>8
    \tuplet 6/4 {r16  b[ c d e d]} |
  <c d,>8 <c d,> \tuplet 6/4 {r16  a[ b c d c]} <b d, g,>8 <b d, g,>
    \tuplet 6/4 {r16  g'[ fis g g, b]} |
  %5
  <a e c>8[ \tuplet 3/2 {c16 d e]} \tuplet 6/4 { d[ c b a g fis]} g8 g,
    \tuplet 6/4 { g16[ g g g g g] |
  <c' e, g,>[ <e, g,> <e g,> <e g,> <e g,> <e g,>]
    <b' d, g,>[ <d, g,> <d g,> <d g,> <d g,> <d g,>]  a'[ g fis c' b a]}
     g32[ a g a b16 b] |
  %7
   c32[ d e d e d c b]  a[ b c b c b a g] g8 fis \tuplet 6/4 { fis16[ e d fis e d] |
   a[ a' a, a g' a,]  a[ a' a a a a]}
  % Note: the following 4/6 section was written as
  % times 6/6 {cis,4 d | <b' g> <e, cis> <a fis> <d, b> | e}
  % i.e. as quarters with 6s above, everything without stems.
  % The last figure must, in its context, be  e[ g g g g g]
    \tuplet 6/4 {cis,16 a' a a a a d, a' a a a a |
  %9
  g b b b b b cis, e e e e e fis a a a a a b, d d d d d |
  e g g g g g} \tuplet 6/4 { a,16[ cis' d e d cis] <fis, a,>[ d' e fis e d]  cis,[ e' fis g fis e]} |
  %11
   d8[ d,]  fis[ d]  g[ \tuplet 3/2 {g'16 e g]} \tuplet 6/4 { fis[ d fis e cis e]} |
   d8[ d, fis d] g \tuplet 3/2 { g'16[ e g]} \tuplet 6/4 { fis[ d fis e cis e]} |
  %13
   d8[ \tuplet 3/2 {a16 a a]} \tuplet 6/4 {<a fis>[-. <a fis>-. <a fis>-.
    <a fis>-. <a fis>-. <a fis>-.]} <a fis>4 r^\fermata |
  }

  %14
  \repeat "volta" 2 {
  <fis' a, d,>8 <fis a, d,> \tuplet 6/4 {r16  d[ e fis g fis]} <e cis>8 <e cis>
    \tuplet 6/4 {r16  cis[ d e fis e]}
  <d fis, a,>8 <d fis, a,> \tuplet 6/4 {<a fis>16[ <a fis> <a fis> <a fis>
    <a fis> <a fis>]} <b g g,>8 \tuplet 3/2 { g16[ a b]}
    \tuplet 6/4 { a[ g fis e fis g]} |
  %16
   fis8[ d] <b' gis>[ <b gis>]  a[ a,] <a' fis>[ <a fis>] |
  g g,  b'16[ b a g] g8 fis d'16 f e d |
  %18 page 3
  d8 cis c16 e d c c8 b g'16 d c b |
  <<{ b8[ a]  d16[ c b a]} \\ {d,4 d}>> <b' d, g,>8 <b d, g,>
    \tuplet 6/4 {r16  g[ a b c b]} |
  %20
  <a fis>8 <a fis> \tuplet 6/4 {r16  fis[ g a b a]} <g g,>8 <g g,>
    \tuplet 6/4 {r16  b[ a b b, d]} |
  <a' e c>8[ \tuplet 3/2 {a16 b c]} \tuplet 6/4 { b[ a g c, b a]} b8 g r4 |
  %22
  <d'' d, g,>8 <d d, g,>8 \tuplet 6/4 {r16  bes[ c d es d]} <c d,>8 <c d,>
    \tuplet 6/4 {r16  a[ bes! c d c]} |
  <b d,>8 <b d,> \tuplet 6/4 {r16  g'[ fis g g, bes]} <a es c>8[
    \tuplet 3/2 {c16 d es]} \tuplet 6/4 { d[-. c-. bes!-. a-. g-. fis-.]} |
  %24
  g8 g, <g' b,> <g b,> <e c> <e c> <g b,> <g b,> |
  <e c> <e c>  a'32[ g fis! g g fis eis fis]  fis[ d cis d eis d cis d]
     a'[ g fis! g g fis eis! fis] |
  %26
   fis[ d cis d e d cis d] fis,8 d d d <a' fis> <a fis> |
  <g g,>4 r8 d16 fis e g fis a g b a c |
  %28
   b8[ \tuplet 3/2 {e16 d c]} \tuplet 6/4 { b[ fis-. g-. a-. b-. a-.]} <g g,>8
    \tuplet 3/2 { e'16[ d c]} \tuplet 6/4 { b[ fis g a b a]} |
  <g g,>8[ \tuplet 3/2 {<g g,>16 b g]} \tuplet 6/4 {<g g,>[ b g
    <g g,> b g]} <g g,>8[ \tuplet 3/2 {c16 b a]}
    \tuplet 6/4 { g[ a b a g fis]} |
  %30
  g8 d16 g e g fis a g b a c b d c e |
   d8[ \tuplet 3/2 {g16 fis e]} \tuplet 6/4 {<d d,>[ <a d,> <b d,> <c d,>
    <d d,> <c d,>]} <b d, g,>8 \tuplet 3/2 { g'16[ fis e]} \tuplet 6/4 {<d d,>[ <a d,> <b d,> <c d,> <d d,> <c d,>]} |
  %32
  <b d, g,>8[ \tuplet 3/2 {b16 d b]} \tuplet 6/4 { b16[ d b b16 d b]}
    <b d, g,>8[ \tuplet 3/2 {e16 d c]} \tuplet 6/4 { b[ c d c b a]} |
  <g g,>8 b, c a   b[ \tuplet 3/2 {e'16 d c]} \tuplet 6/4 { b[ a g fis g a]} | 
  %34
  g8 b, c a  b[ \tuplet 3/2 {e'16 d c]} \tuplet 6/4 { b[ a g fis g a]} |
  g8 b, c a  b[ \tuplet 3/2 {e'16 d c]} \tuplet 6/4 { b[ a g c, b a]} |
  %36
   g8[ \tuplet 3/2 {<d' b>16 <d b> <d b>]} \tuplet 6/4 {<d b>[ <d b> <d b>
    <d b> <d b> <d b>]} <g' b, d, g,>4 s4 \bar "|."
  }
}
