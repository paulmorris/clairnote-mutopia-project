\version "2.19.49"

\header {
  title ="Sonata a due Mandolini"
  composer = "Emmanuele Barbella (1718-1777)"
  instrument = \Instrument
  mutopiatitle ="Sonata a Due Mandolini (Gimo 13)"
  mutopiacomposer = "BarbellaE"
  mutopiainstrument = "2 Mandolins"
  date = "c.1760"
  mutopiadate = "c. 1760"
  source = "Gimo Music Collection"
  style = "Classical"
  license = "Public Domain"
  maintainer = "Erik Sandberg"
  maintainerEmail = "ersa9195@student.uu.se"

 footer = "Mutopia-2013/04/10-318"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}
