\version "2.19.49"

\header {
  title = "Trio a due mandolini e Basso"
  mutopiatitle = "Trio a due mandolini e Basso (Gimo 359)"
  composer = "Anonymous"
  mutopiacomposer = "Anonymous"
  mutopiainstrument = "2 Mandolins, Bass"
  date = "c.1760"
  mutopiadate = "c. 1760"
  source = "Gimo Music Collection"
  style = "Baroque"
  license = "Public Domain"
  maintainer = "Erik Sandberg"
  maintainerEmail = "ersa9195@student.uu.se"
  instrument = \Instrument

 footer = "Mutopia-2013/04/10-334"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Placed in the \with-url #"http://creativecommons.org/publicdomain/zero/1.0/" {public domain} by the typesetter. Free to distribute, modify, and perform.}}
 tagline = ##f
}
