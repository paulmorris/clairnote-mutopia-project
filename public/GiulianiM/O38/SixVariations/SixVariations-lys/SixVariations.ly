\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\header {
  title = "Six Variations for Guitar"
  composer = "Mauro Giuliani"
  opus = "Op. 38"
  mutopiainstrument = "Guitar"
  mutopiacomposer = "GiulianiM"
  style = "Classical"
  date = "c.1812"
  source = "Imprint: a Vienne chez Artaria et Comp. No. 2264"
  % Boije 234
  license = "Creative Commons Attribution-ShareAlike 3.0"
  maintainer = "Glen Larsen"
  maintainerEmail = "glenl.glx@gmail.com"

 footer = "Mutopia-2013/07/07-1844"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Licensed under \with-url #"http://creativecommons.org/licenses/by-sa/3.0/" {Creative Commons Attribution-ShareAlike 3.0} Free to distribute, modify, and perform.}}
 tagline = ##f
}

\paper {
  top-margin = 10
}

\include "thema.ly"
\include "vari.ly"
\include "varii.ly"
\include "variii.ly"
\include "variv.ly"
\include "varv.ly"
\include "varvi.ly"
