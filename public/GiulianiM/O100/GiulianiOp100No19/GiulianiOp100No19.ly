\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\header {
  title = "24 Studies for the Guitar"
  mutopiatitle = "24 Studies for the Guitar, No. 19"
  source = "Statens musikbibliotek - The Music Library of Sweden"
  composer = "Mauro Giuliani"
  opus = "Op. 100"
  piece = "No. 19. Prelude."
  mutopiacomposer = "GiulianiM"
  mutopiainstrument = "Guitar"
  style = "Classical"
  license = "Creative Commons Attribution-ShareAlike 3.0"
  maintainer = "Glen Larsen"
  maintainerEmail = "glenl at glx.com"
 footer = "Mutopia-2012/01/14-1819"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Licensed under \with-url #"http://creativecommons.org/licenses/by-sa/3.0/" {Creative Commons Attribution-ShareAlike 3.0} Free to distribute, modify, and perform.}}
 tagline = ##f
}

\layout {
  indent = 60\pt
  short-indent = 0\pt
  ragged-last-bottom = ##t
}

posI = \markup{"I"}
posII = \markup{"II"}
posIII = \markup{"III"}
posIV = \markup{"IV"}
posV = \markup{"V"}
posVI = \markup{"VI"}
posVII = \markup{"VII"}
posVIII = \markup{"VIII"}

global = {
  \time 4/4
  \key g \major
  \set Staff.autoBeaming = ##f
}

upperVoice = \relative c {
  \voiceOne
  \slurDown

  g8_\mf[ a16( b] c)[ d( e fis)] g([ a) b( c] d)[ e fis^\posVII( g) ] |
  fis16([ g fis) e] fis([ g a) b] c([ b) a( g)] fis[ e( d) c] |

  b16[ g b d] g[ b, d g] b[ d, g b] d8.[ b16] |
  a[ c g b] fis16[ a e g] d^\posV[ fis c^\posIII e] b^\posII[ d a c^\posI] |
  <g b>16[ g,( a b] c)[ d( e fis)] g16([ a) (b c] d)[ e fis g] |

  fis16([ g fis) e] fis([ g a) b] c([ b) a( g)] fis[ e( d) c] |
  b16[ g b d] g[ b, d g] b[ d, g b] d8.[ b16] |
  a16[ c g b] fis^\posV[ a e^\posIII g] d[^\posII fis c e^\posI] b[ d a c] |

  <g b>8[ g,16 a8(] b16[ c) d( e] f)[ f'16( e)] f([ e) d( c)] |
  b8[ g16( a)] b[ c d e] f([ e) d( c)] b([ a g) f] |

  e16[ c e g] c[ e, g c] e[ g, c e] g8[ g] |
  bes8^\posIII[ g16( bes)] e,16^\posII[ g cis, e] bes[ cis g bes] e,[ g cis, e] |
  r4^\posI <g b g'>4 r4 <a d fis> |
  <g b g'>4 r4 r2
  \bar "|."
}

lowerVoice = \relative c {
  \voiceTwo
  \repeat unfold 4 {
    R1 |
    d4 r d r |
  }
  % slide the rest over
  \once \override MultiMeasureRest.extra-offset = #'( 8 . 0 )
  R1 |
  \repeat unfold 3 { R1 | }
  d4 r d r |
  g,4 r r2 |
}

\score {
  <<
    \new Staff = "Guitar"
    <<
      \set Staff.instrumentName = #"Vivace."
      \set Staff.midiInstrument = #"acoustic guitar (nylon)"
      \mergeDifferentlyHeadedOn
      \mergeDifferentlyDottedOn
      \clef "treble_8"
      \global
      \context Voice = "upperVoice" \upperVoice
      \context Voice = "lowerVoice" \lowerVoice
    >>
  >>
  \layout {}
  \midi {
    \tempo 4 = 100
  }
}
