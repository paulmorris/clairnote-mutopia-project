\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\header {
  title = "24 Studies for the Guitar"
  mutopiatitle = "24 Studies for the Guitar, No. 23"
  source = "Statens musikbibliotek - The Music Library of Sweden"
  composer = "Mauro Giuliani"
  opus = "Op. 100"
  piece = "No. 23. Prelude."
  mutopiacomposer = "GiulianiM"
  mutopiainstrument = "Guitar"
  style = "Classical"
  license = "Creative Commons Attribution-ShareAlike 3.0"
  maintainer = "Glen Larsen"
  maintainerEmail = "glenl at glx.com"
 footer = "Mutopia-2012/01/14-1823"
 copyright = \markup { \vspace #1.8 \sans \abs-fontsize #7.5 \wordwrap {Sheet music in \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation}  published by Paul Morris using \with-url #"http://www.lilypond.org" {LilyPond.} Original typesetting by \maintainer for the \with-url #"http://www.mutopiaproject.org" {Mutopia Project.} Licensed under \with-url #"http://creativecommons.org/licenses/by-sa/3.0/" {Creative Commons Attribution-ShareAlike 3.0} Free to distribute, modify, and perform.}}
 tagline = ##f
}

\layout {
  indent = 60\pt
  short-indent = 0\pt
  ragged-last-bottom = ##f
}

global = {
  \time 4/4
  \key a \major
}


upperVoice = \relative c' {
  \voiceOne
  \slurDown
  r16^II cis[ e cis] a[ cis e cis] r d[ e d] b[ d e d] |
  r16 e^II[ e e] cis[ e e e] r cis^II[ e cis] a[ cis e cis] |

  r16 b[ e b] gis[ b e b] r cis[ e cis] a[ cis e cis] |
  r16 d[ e d] b[ d e d] r b[ e b] gis[ b e b] |
  r16 e,[ a cis] r e,[ a cis] r e,[ a cis] r e,[ gis d'] |

  r16 e,[ a cis] r e,[ a cis] r e,[ a cis] r e,[ gis d'] |
  r16 e,[ a cis] r e,[ cis' e] r e,[ b' d] r e,[ a cis] |
  b[ e,, b' e] gis[ b, e gis] b[ e, gis b] e8[ e] |

  r16^V a,[ cis e] a,[ cis e a] r a,[ d fis] a,[ d fis a] |
  r16 a,[ cis e] a,[ cis e e] a,[ cis e e] a,[ cis e e] |
  r16 a,[ cis e] a,[ cis e a] r a,[ d fis] a,[ d fis a] |

  r16 a,[ cis e] a,[ cis e e] a,[ cis e e] a,[ cis e e] |
  r16 b^II[ d e] r b[ d e] r b[ d e] r b[ d e] |
  r16 a,[ cis e] r a,[ cis e] r a,[ cis e] r a,[ cis e] |

  r16 a,^I[ b fis'] r a,[ b fis'] r gis,[ b e] r gis,[ b e] |
  r16 a,^II[ cis a] r b[ d b] r cis[ e cis] r d[ fis d] |
  r16 cis[ e cis] a'[ cis, e cis] r gis[ b gis] e'[ gis, b gis] |

  r16 a[ cis a] r b[ d b] r cis[ e cis] r d[ fis d] |
  r16 cis[ e cis] a'[ cis, e cis] r gis[ b gis] e'[ gis, b gis] |
  r16 a[ cis a] e'[ a, cis a] r a[ cis a] r a[ cis a] |


  r16 a[ cis a] e'[ a, cis a] r a[ cis a] r a[ cis a] |
  r2 <a cis e a>4 r |
  <cis, e a>2 r

  \bar "|."
}

lowerVoice = \relative c {
  \voiceTwo
  % this tweak gets the dynamic moved out of the way of the mf below it
  \once\override DynamicText.extra-offset = #'( -4 . 4)
  a2\mf a |
  a2 a |

  e2 e |
  e2 e |
  a4 a a a |

  a4 a a a |
  a4 a a a |
  R1 |

  \once\override DynamicText.extra-offset = #'( -3 . 3)
  a2\f a |
  << { a2 d2\rest } \\ {
    \stemDown\autoBeamOff s4.. e'16 s8. e16 s8. e16} >> |
  a,,2 a |

  << { a2 d2\rest } \\ {
    \stemDown\autoBeamOff s4.. e'16 s8. e16 s8. e16} >> |
  e,,4 gis b e, |
  a cis e a, |

  d4 d e e |
  a,4 a a a |
  a2 e |

  a4 a a a |
  a2 e |
  a2 e4 cis' |

  a2 e4 cis' |
  a4 r a r |
  \once\override DynamicText.extra-offset = #'( -3 . 3)
  a2\f r
}

\score {
  <<
    \new Staff = "Guitar"
    <<
      \global
      \set Staff.instrumentName = #"Allegro."
      \set Staff.midiInstrument = #"acoustic guitar (nylon)"
      \mergeDifferentlyHeadedOn
      \mergeDifferentlyDottedOn
      \clef "treble_8"
      \context Voice = "upperVoice" \upperVoice
      \context Voice = "lowerVoice" \lowerVoice
    >>
  >>
  \layout {}
  \midi {
    \tempo 4 = 100
  }
}
